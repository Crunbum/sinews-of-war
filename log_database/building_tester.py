import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys

def data_verifier (start_year,end_year,counties,duchies,kingdoms,empires):
    df_pop = pd.read_csv('pop_data.csv',delimiter = ';')
    data_years = df_pop['Year'].unique()
    data_county = df_pop['County'].unique()
    data_duchy = df_pop['Duchy'].unique()
    data_kingdom = df_pop['Kingdom'].unique()
    data_empire = df_pop['Empire'].unique()
    for county in counties:
        if county not in data_county:
            print(f'{county} not in database')
    for duchy in duchies:
        if duchy not in data_duchy:
            print(f'{duchy} not in database')
    for kingdom in kingdoms:
        if kingdom not in data_kingdom:
            print(f'{kingdom} not in database')
    for empire in empires:
        if empire not in data_empire:
            print(f'{empire} not in database')
    #if not (str(start_year) in data_years):
    #    print(f'{start_year} out of range')
    #if not (str(end_year) in data_years):
    #    print(f'{end_year} out of range')
    
if __name__ == '__main__':
    start_year = 1067
    end_year = 1444
    counties = []
    duchies = []
    kingdoms = []
    empires = []
    title = ''
    for argument in sys.argv[1:]:
        argument.strip()
        if start_year == 1067 and argument.isnumeric() :
            start_year = argument
        elif argument.isnumeric():
            end_year = argument
        elif argument.startswith('c_'):
            counties.append(argument)
            title = title + ' ' + argument
        elif argument.startswith('d_'):
            duchies.append(argument)
            title = title + ' ' + argument
        elif argument.startswith('k_'):
            kingdoms.append(argument)
            title = title + ' ' + argument
        elif argument.startswith('e_'):
            empires.append(argument)
            title = title + ' ' + argument
        else:
            print (f"Invalid Argument: {argument}")

    data_verifier(start_year,end_year,counties,duchies,kingdoms,empires)
    query_year = np.arange(start = int(start_year), stop = int(end_year) + 1, step = 1)

    pop = pop_query_tool(query_year,counties,duchies,kingdoms,empires,'pop',title)
    urban_pop = pop_query_tool(query_year,counties,duchies,kingdoms,empires,'urban_pop',title)
    rural_pop = np.subtract(pop,urban_pop)
    plot_total(query_year,rural_pop,urban_pop,pop,title)
