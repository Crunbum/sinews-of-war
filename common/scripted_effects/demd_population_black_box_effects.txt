﻿#############################################
# DEMD Population System
# by Vertimnus (NaniteSystems)
# Do not edit without making corresponding edits to the metascript source code. Bad things will happen!
#############################################
reset_migration_pulls = {
	every_county = {
		set_variable = { name = migration_pull value = migration_pull_target }
		every_county_province = {  limit = { combined_building_level > 0 }
			set_variable = { name = migration_pull value = migration_pull_target }
		}
	}
}
top_up_urban_pop = {
	every_county = {
		set_variable = { 
			name = population 
			value = {
				value = var:food_consumed
				multiply = 0.9
			}
		}
	}
}

top_up_goods_districts = {
	every_county = {
		# set up goods districts
		set_variable = {
			name = goods_districts_count
			value = {
				value = var:population
				divide = demd_jobs_per_district
			}
		}
		clamp_variable = { name = goods_districts_count max = 1000 min = 1 }
	}
}

top_up_goods_stockpiles = {
	every_county = {
		set_variable = {
			name = goods_stockpile
			value = {
				value = var:goods_produced
				divide = goods_depreciation_rate
			}
		}
		every_county_province = {  limit = { combined_building_level > 0 }
			set_variable = {
				name = goods_stockpile
				value = {
					value = var:goods_imported
					divide = goods_depreciation_rate
				}
			}
		}
	}
}

demd_initialize_new_holding = {
	set_variable = { name = population value = 0.5 }
	set_variable = { name = food_districts_max value = prov_food_districts }
	set_variable = { name = food_producers_count value = 0.5 }
	set_variable = { name = food_districts_count value = 1 }
	set_variable = { name = migration_pull value = migration_pull_target }
	set_variable = { name = food_stockpile value = 1 }
	set_variable = { name = goods_stockpile value = 1 }
}

demd_raid_effect = {
	scope:barony.title_province = {
		demd_alter_prov_population = { amount = small operation = subtract }
		demd_alter_food_district_count = { amount = medium operation = subtract }
		demd_alter_food_stockpile = { amount = medium operation = subtract }
		county = { change_county_control = -10 }
	}
}

demd_occupy_effect = {
	scope:barony.title_province = {
		demd_alter_prov_population = { amount = small operation = subtract }
		demd_alter_goods_district_count = { amount = medium operation = subtract }
		demd_alter_goods_stockpile = { amount = medium operation = subtract }	
		demd_alter_food_stockpile = { amount = medium operation = subtract }	
		county = {
			add_county_modifier = {	#Prevent the county from being sacked again for quite a while.
				modifier = recently_sacked_modifier
				years = 20
			}
			change_county_control = -25	
		}
		
	}
}

demd_sack_effect = {
	scope:barony.title_province = {
		demd_alter_prov_population = { amount = large operation = subtract }
		demd_alter_goods_district_count = { amount = large operation = subtract }
		demd_alter_goods_stockpile = { amount = large operation = subtract }	
		demd_alter_food_stockpile = { amount = large operation = subtract }	
		county = {
			add_county_modifier = {	#Prevent the county from being sacked again for quite a while.
				modifier = recently_sacked_modifier
				years = 20
			}
			change_county_control = -50
		}
		
		county.holder = {
			every_liege_or_above = {
				if = {
					limit = {
						has_opinion_modifier = {
							target = scope:occupier
							modifier = sacked_me_opinion
						}
					}
					remove_opinion = { # We reapply it so that the counter is reset
						target = scope:occupier
						modifier = sacked_me_opinion
					}
				}
				add_opinion = {
					target = scope:occupier
					modifier = sacked_me_opinion
				}
			}
		}
	}
}

demd_raze_effect = {
	scope:barony.title_province = {
		demd_alter_prov_population = { amount = huge operation = subtract }
		demd_alter_goods_district_count = { amount = huge operation = subtract }
		demd_alter_goods_stockpile = { amount = huge operation = subtract }	
		demd_alter_food_stockpile = { amount = huge operation = subtract }	
		county = {
			add_county_modifier = {	#Prevent the county from being sacked again for quite a while.
				modifier = recently_sacked_modifier
				years = 20
			}
			change_county_control = -75
		}		

		county.holder = {
			every_liege_or_above = {
				if = {
					limit = {
						has_opinion_modifier = {
							target = scope:occupier
							modifier = razed_me_opinion
						}
					}
					remove_opinion = { # We reapply it so that the counter is reset
						target = scope:occupier
						modifier = razed_me_opinion
					}
				}
				add_opinion = {
					target = scope:occupier
					modifier = razed_me_opinion
				}
			}
		}
	}
}

#####################################################
# type = food, goods
# amount = miniscule, small, medium, large, huge
# sample usage: reduceDistrictCount = { type = food amount = small operation = subtract }


demd_alter_county_food_district_count = {
	custom_tooltip = demd_$operation$_food_districts_$amount$
	every_county_province = {
		change_variable = {
			name = food_districts_count
			multiply = {
				value = 1
				$operation$ = demd_district_change_$amount$
			}
		}
		clamp_variable = { name = food_districts_count min = 0 max = 1000000 }
	}
}


demd_alter_food_district_count = {
	custom_tooltip = demd_$operation$_food_districts_$amount$
	if = {
		limit = { has_variable = food_districts_count }	
		
		change_variable = {
			name = food_districts_count
			multiply = {
				value = 1
				$operation$ = demd_district_change_$amount$
			}
		}
		clamp_variable = { name = food_districts_count min = 0 max = 1000000 }
	}
}

# use in prov scope
demd_alter_goods_district_count = {	
	if = {
		limit = { county.title_province = this }
		custom_tooltip = demd_$operation$_goods_districts_$amount$		
		county = {
			change_variable = {
				name = goods_districts_count
				multiply = {
					value = 1
					$operation$ = demd_district_change_$amount$
				}
			}
			clamp_variable = { name = goods_districts_count min = 0 max = 1000000 }
		}
	}
}

#####################################################
# Scope: landed title (node)
# operation = add, subtract
# amount = miniscule, small, medium, large, huge
# sample usage: demd_alter_prov_population = { amount = small operation = subtract }

# use in county scope
demd_alter_county_population = {
	custom_tooltip = demd_$operation$_population_$amount$
	
	change_variable = {
		name = population
		multiply = {
			value = 1
			$operation$ = demd_population_change_$amount$
		}
	}
	clamp_variable = { name = population min = 0.1 max = 1000000 }
	
	every_county_province = {
		if = {
			limit = { has_variable = population }
			
			change_variable = {
				name = population
				multiply = {
					value = 1
					$operation$ = demd_population_change_$amount$
				}
			}
			clamp_variable = { name = population min = 0.1 max = 1000000 }
		}
	}
}

# use in county scope
demd_alter_urban_population = {
	custom_tooltip = demd_$operation$_urban_population_$amount$
		
	if = {
		limit = { has_variable = population }
		
		change_variable = {
			name = population
			multiply = {
				value = 1
				$operation$ = demd_population_change_$amount$
			}
		}
		clamp_variable = { name = population min = 0.1 max = 1000000 }
	}
}

# use in province scope
demd_alter_prov_population = {
	custom_tooltip = demd_$operation$_population_$amount$
	
	if = {
		limit = { county.title_province = this }
		county = {
			change_variable = {
				name = population
				multiply = {
					value = 1
					$operation$ = demd_population_change_$amount$
				}
			}
			clamp_variable = { name = population min = 0.1 max = 1000000 }
		}
	}	
	if = {
		limit = { has_variable = population }
		
		change_variable = {
			name = population
			multiply = {
				value = 1
				$operation$ = demd_population_change_$amount$
			}
		}
		clamp_variable = { name = population min = 0.1 max = 1000000 }
	}
}

#####################################################
# Scope: landed title (node)
# type = food, goods
# operation = add, subtract
# amount = small, medium, large, huge
# sample usage: demd_alter_resource_stockpile = { type = food operation = add amount = small }

# use in prov scope
demd_alter_food_stockpile = {
	custom_tooltip = demd_$operation$_food_stockpile_$amount$
	
	if = {
		limit = { county.title_province = this }
		county = {
			change_variable = { 
				name = food_stockpile 
				multiply = {
					value = 1
					$operation$ = demd_resource_stockpile_gain_$amount$
				}
			}
		}
	}		
	change_variable = { 
		name = food_stockpile 
		multiply = {
			value = 1
			$operation$ = demd_resource_stockpile_gain_$amount$
		}
	}	
}

# use in prov scope
demd_alter_goods_stockpile = {
	custom_tooltip = demd_$operation$_goods_stockpile_$amount$
	if = {
		limit = { county.title_province = this }
		county = {
			change_variable = { 
				name = goods_stockpile 
				multiply = {
					value = 1
					$operation$ = demd_resource_stockpile_gain_$amount$
				}
			}
		}
	}
		
	change_variable = { 
		name = goods_stockpile 
		multiply = {
			value = 1
			$operation$ = demd_resource_stockpile_gain_$amount$
		}
	}
}

#####################################################
# Scope: landed title (node)
# type = faith, culture
# direction = attack, defense
# amount = small, medium, large, huge
# sample usage: demd_add_faith_strength_attack = { amount = 0.1 }

demd_alter_conversion_power = {
	custom_tooltip = demd_$operation$_$type$_strength_$direction$_$amount$
	if = {
		limit = { has_variable = $type$_strength_$direction$ }
		
		change_variable = { name = $type$_strength_$direction$ $operation$ = demd_strength_change_$amount$ }
	}
}

#####################################################
# Scope: landed title (node)
# type = faith, culture
# direction = attack, defense
# amount = small, medium, large, huge
# sample usage: demd_add_faith_strength_attack = { amount = 0.1 }

demd_alter_conversion_progress = {
	custom_tooltip = demd_$operation$_$type$_conversion_$amount$
	change_variable = { name = $type$_conversion_progress $operation$ = demd_conversion_change_$amount$ }
}

#####################################################
# Scope: node (landed title)
# amount = small, medium, large, huge
# operation = add, subtract
# sample usage: demd_alter_node_weather = { amount = small operation = add }
demd_alter_node_weather = {
	custom_tooltip = demd_$operation$_node_weather_$amount$
	if = {
		limit = { has_variable = node_weather_fertility_mult }
		
		change_variable = { name = node_weather_fertility_mult $operation$ = demd_fertility_change_$amount$ }

	}
}

#####################################################
# Scope: county
# amount = small, medium, large, huge
# operation = add, subtract
# sample usage: demd_alter_county_weather = { amount = small operation = add }
demd_alter_county_weather = {
	custom_tooltip = demd_$operation$_county_weather_$amount$
	change_variable = { name = county_weather_fertility_mult $operation$ = demd_fertility_change_$amount$ }
}

#####################################################
# Scope: county
# amount = small, medium, large, huge
# operation = add, subtract
# sample usage: demd_alter_sanitation = { amount = small operation = add }
demd_alter_sanitation = {
	custom_tooltip = demd_$operation$_sanitation_$amount$
	if = {
		limit = { has_variable = sanitation }
		
		change_variable = { name = sanitation $operation$ = demd_sanitation_change_$amount$ }
		
		clamp_variable = { name = sanitation min = 0 max = 1 }		
	}
}

#####################################################
# Scope: county
# amount = small, medium, large, huge
# operation = add, subtract
# sample usage: demd_alter_public_order = { amount = small operation = add }
demd_alter_public_order = {
	custom_tooltip = demd_$operation$_public_order_$amount$
	if = {
		limit = { has_variable = public_order }
		
		change_variable = { name = public_order $operation$ = demd_public_order_change_$amount$ }
		
		clamp_variable = { name = public_order min = 0 max = 1 }		
	}
}

