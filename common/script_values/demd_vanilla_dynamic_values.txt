﻿battle_winner_prestige_gain = {
	value = 100
	
	if = { # Bonus for tribals
		limit = {
			has_government = tribal_government
		}
		add = 100
	}
	
	if = { # Leading the troops yourself adds a bit
		limit = {
			OR = {
				scope:combat = {
					combat_attacker = {
						exists = side_commander
						side_commander = {
							this = root
						}
					}
				}
				scope:combat = {
					combat_defender = {
						exists = side_commander
						side_commander = {
							this = root
						}
					}
				}
			}
		}
		add = 50
	}
	
	if = { # Bonus for legacy
		limit = {
			exists = dynasty
			dynasty = {
				has_dynasty_perk = fp1_pillage_legacy_1
			}
		}
		multiply = 2
	}
	multiply = 0.2
}

battle_winner_piety_gain = {
	value = 0
	if = {
		limit = {
			faith = {
				faith_hostility_level = {
					target = scope:enemy_battle_owner.faith
					value >= religious_cb_enabled_hostility_level
				}
			}
		}
		value = 50
		
		if = { # Bonus for tribals
			limit = {
				has_government = tribal_government
			}
			add = 50
		}
		
		if = { # Leading the troops yourself adds a bit
			limit = {
				OR = {
					scope:combat = {
						combat_attacker = {
							exists = side_commander
							side_commander = {
								this = root
							}
						}
					}
					scope:combat = {
						combat_defender = {
							exists = side_commander
							side_commander = {
								this = root
							}
						}
					}
				}
			}
			add = 25
		}
	}
	multiply = 0.2
}
battle_winner_gold_gain = {
	value = 0
	
	if = { # Bonus for legacy
		limit = {
			exists = dynasty
			dynasty = {
				has_dynasty_perk = fp1_pillage_legacy_4
			}
		}
		add = 50
	}
	multiply = 0.2
}