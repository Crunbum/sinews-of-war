﻿barony_is_valid_for_holy_order_lease_trigger = {
	trigger_if = {
		limit = {holder = $CHARACTER$}
		can_be_leased_out = yes
	}
	trigger_else = {holder.primary_title.tier = tier_barony}
}