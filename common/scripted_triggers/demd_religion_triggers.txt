﻿suitable_faith_for_undirected_ghw_trigger = {
	OR = {
		has_doctrine_parameter = great_holy_wars_active
		AND = {
			has_doctrine_parameter = great_holy_wars_active_if_reformed
			NOT = { has_doctrine_parameter = unreformed }
		}
		has_doctrine = divine_destiny_doctrine #SPECIAL: Has become the Chakravarti
	}
	has_doctrine = doctrine_spiritual_head
	NOR = {
		has_doctrine = unreformed_faith_doctrine
		AND = {
			has_doctrine = tenet_dharmic_pacifism
			NOT = { has_doctrine = divine_destiny_doctrine }
		}
		has_doctrine = tenet_pacifism
	}
	fervor >= 60
	save_temporary_scope_value_as = {
		name = faith_strength
		value = faith_military_strength
	}
	save_temporary_scope_as = faith
	any_kingdom = {
		valid_ghw_target_trigger = { FAITH = scope:faith }
	}
}

suitable_faith_for_directed_ghw_trigger = {
	OR = {
		has_doctrine = tenet_armed_pilgrimages #Catholics
		has_doctrine = tenet_struggle_submission #Islam (though, the default Faiths have Temporal Heads, so still do not qualify)
		has_doctrine = tenet_warmonger #Germanic (though, starting as Unreformed, it still doesn't qualify by default)
		has_doctrine = divine_destiny_doctrine #SPECIAL: Has become the Chakravarti
	}	
	has_doctrine = doctrine_temporal_head
	NOR = {
		has_doctrine = unreformed_faith_doctrine
		AND = {
			has_doctrine = tenet_dharmic_pacifism
			NOT = { has_doctrine = divine_destiny_doctrine }
		}
		has_doctrine = tenet_pacifism
	}
	fervor >= 60
}

suitable_faith_for_any_ghw_trigger = {
	OR = {
		has_doctrine = tenet_armed_pilgrimages #Catholics
		has_doctrine = tenet_struggle_submission #Islam (though, the default Faiths have Temporal Heads, so still do not qualify)
		has_doctrine = tenet_warmonger #Germanic (though, starting as Unreformed, it still doesn't qualify by default)
		has_doctrine = divine_destiny_doctrine #SPECIAL: Has become the Chakravarti
	}
	OR = {
		has_doctrine = doctrine_temporal_head
		has_doctrine = doctrine_spiritual_head
	}
	NOR = {
		has_doctrine = unreformed_faith_doctrine
		AND = {
			has_doctrine = tenet_dharmic_pacifism
			NOT = { has_doctrine = divine_destiny_doctrine }
		}
		has_doctrine = tenet_pacifism
	}
	fervor >= 50
}