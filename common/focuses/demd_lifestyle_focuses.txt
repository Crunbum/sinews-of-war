﻿
# nerfed a bit
martial_chivalry_focus = {
	lifestyle = martial_lifestyle

	desc = {
		desc = martial_chivalry_focus_desc
		desc = line_break
	}
	
	modifier = {
		advantage = 5
		prowess = 2
		attraction_opinion = 10
	}

	auto_selection_weight = {
		value = 11
		if = {
			limit = {
				has_education_martial_trigger = yes
			}
			add = 1989
		}
		if = {
			limit = {
				has_trait = brave
			}
			multiply = 5
		}
		if = {
			limit = {
				has_trait = honest
			}
			multiply = 2
		}
		if = {
			limit = {
				has_trait = chaste
			}
			multiply = 1.5
		}
	}

	focus_id = 5
}

# removed huge domain limit gain
stewardship_domain_focus = {
	lifestyle = stewardship_lifestyle

	desc = {
		desc = stewardship_domain_focus_desc
		desc = line_break
	}
	
	modifier = {
		domain_limit = 1
	}

	auto_selection_weight = {
		value = 11
		if = {
			limit = {
				has_education_stewardship_trigger = yes
			}
			add = 1989
		}
		if = {
			limit = {
				has_trait = diligent
			}
			multiply = 2
		}
	}

	focus_id = 10
}

learning_scholarship_focus = {
	lifestyle = learning_lifestyle

	desc = {
		desc = learning_scholarship_focus_desc
		desc = line_break
	}
	
	modifier = {
		learning = 3
		development_growth_factor = 0.15
	}

	auto_selection_weight = {
		value = 11
		if = {
			limit = {
				has_education_learning_trigger = yes
			}
			add = 1989
		}
	}

	focus_id = 13
}