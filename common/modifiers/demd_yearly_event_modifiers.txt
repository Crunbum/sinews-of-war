﻿yearly_reduced_theocracy_levies_modifier = {
	icon = martial_negative
	# DEMD-LEV
	army_maintenance_mult = 0.03 
}
foreign_trader_enclaves_saharan_modifier = {
	icon = stewardship_positive
	development_growth_factor = large_development_growth_gain
	monthly_county_control_change_factor = 1
	supply_capacity_mult = 0.5
}

foreign_traders_unable_to_root_saharan_modifier = {
	icon = stewardship_negative
	development_growth_factor = small_development_growth_loss
	tax_mult = -0.2
}
local_recession_epicentre_saharan_modifier = {
	icon = stewardship_negative
	development_growth_factor = medium_development_growth_loss
	tax_mult = -0.4
	county_opinion_add = -20
}

local_recession_ripple_saharan_modifier = {
	icon = stewardship_negative
	development_growth_factor = small_development_growth_loss
	tax_mult = -0.2
}
local_recession_ripple_saharan_modifier = {
	icon = stewardship_negative
	development_growth_factor = small_development_growth_loss
	tax_mult = -0.2
}

integrated_foreign_trader_enclaves_saharan_modifier = {
	icon = stewardship_positive
	#This is intended to be a reduced version of the effects of foreign_trader_enclaves_saharan_modifier.
	development_growth_factor = medium_development_growth_gain
	monthly_county_control_change_factor = 0.5
	supply_capacity_mult = 0.25
}
