﻿focused_modifier = {
	icon = economy_positive
	#monthly_income_mult = 0.1
	build_speed = -0.15
}
rebuilding_marrakesh_modifier = {
	icon = county_modifier_development_positive
	development_growth_factor = 0.25
	build_gold_cost = -0.15
	build_piety_cost = -0.5
}
bactria_royal_tolls_modifier = {
	icon = county_modifier_development_positive
	development_growth = 0.5
	build_speed = -0.1
	build_gold_cost = -0.1
}