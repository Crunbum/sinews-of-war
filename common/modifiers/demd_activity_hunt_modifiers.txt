﻿hunt_peasants_hunted_modifier = {
	icon = hunt_negative
	county_opinion_add = -60
	development_growth_factor = tiny_development_growth_loss
}

hunt_aided_peasants_modifier = {
	icon = hunt_positive
	county_opinion_add = 30
	development_growth_factor = small_development_growth_gain
}