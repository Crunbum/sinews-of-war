package main

import (
	"fmt"
	"os"
	"path/filepath"
)

func main() {

	if len(os.Args) < 2 {
		fmt.Println("Please provide the path to a configuration file as the first argument.")
	} else {

		// read config file and retrieve params
		configInfo := readConfigFile(os.Args[1])

		conceptsFolder := filepath.Join(configInfo.metamodFolder, "concepts")
		scriptedEffectsFolder := filepath.Join(configInfo.metamodFolder, "scriptedEffects")

		// output stuff read from innos/tenets in the metamod scripted effect folder
		fmt.Println("writing inno meta-meta")
		writeInnoScriptedEffect(configInfo.innovationsFolder, scriptedEffectsFolder, "temp_innovations.txt")
		writeTenetScriptedEffect(configInfo.tenetsFolder, scriptedEffectsFolder, "temp_tenets.txt")

		// parse concepts and scripted effects
		fmt.Println("reading concepts")
		concepts := readConceptFiles(conceptsFolder)
		fmt.Println("reading scripted effects")
		scriptedEffectsLib := readScriptedEffectsFiles(scriptedEffectsFolder)
		fmt.Println("expanding metacode")
		scriptedEffectsLib = metaCodeExpander(scriptedEffectsLib, concepts)
		fmt.Println("expanding scripted effects")
		scriptedEffectsLib = scriptedEffectsExpander(scriptedEffectsLib, concepts)

		// delete temporary meta files
		os.Remove(filepath.Join(scriptedEffectsFolder, "temp_innovations.txt"))
		os.Remove(filepath.Join(scriptedEffectsFolder, "temp_tenets.txt"))

		// output things
		fmt.Println("writing annual amortized pulse")
		scriptedEffectsPrinter(scriptedEffectsLib, []string{"annualPulse"}, configInfo.scriptedEffectsOutFolder, "demd_population_annual_pulse.txt")
		fmt.Println("writing monthly amortized pulse")
		scriptedEffectsPrinter(scriptedEffectsLib, []string{"monthlyPulse"}, configInfo.scriptedEffectsOutFolder, "demd_population_monthly_pulse.txt")
		fmt.Println("writing annual un-amortized pulse")
		scriptedEffectsPrinter(scriptedEffectsLib, []string{"tradeSubRoutine", "faithEconomySubRoutine", "cultureEconomySubRoutine",
			"fertilitySubRoutine", "foodDepreciation", "setKnightCapacity", "setRegimentCapacity", "world_migration_end",
			"world_migration_start"}, configInfo.scriptedEffectsOutFolder, "demd_population_year_end_pulse.txt")
		fmt.Println("writing startup pulse")
		scriptedEffectsPrinter(scriptedEffectsLib, []string{"startupPulse"}, configInfo.scriptedEffectsOutFolder, "demd_population_startup_pulse.txt")
		fmt.Println("writing misc functions")
		scriptedEffectsPrinter(scriptedEffectsLib, []string{"setEdicts"}, configInfo.scriptedEffectsOutFolder, "demd_population_single_province_economy.txt")

		// do extra stuff if necessary
		if configInfo.printEvents {
			fmt.Println("writing on actions and events")
			onActionPrinter(configInfo.onActionsFolder, "demd_meta_on_actions.txt")
			eventPrinter(configInfo.eventsFolder, "demd_meta_events.txt")
		}


		if configInfo.processMap {
			fmt.Println("initializing provs")
			landedTitlePath := filepath.Join(configInfo.landedTitleFolder, "00_landed_titles.txt")
			provinceTerrainFile := filepath.Join(configInfo.terrainFolder, "00_province_terrain.txt")
			provinceWinterFile := filepath.Join(configInfo.terrainFolder, "01_province_properties.txt")
			provinceFunction(configInfo.mapDataFolder, provinceTerrainFile, provinceWinterFile, landedTitlePath, configInfo.culturesFolder, configInfo.culturesBackupFolder, configInfo.scriptedEffectsOutFolder, "demd_initializer_effects.txt", configInfo.pixel_exponent)
		}
	}
}