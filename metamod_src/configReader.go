package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type configData struct {
	scriptedEffectsOutFolder string

	mapDataFolder string
	landedTitleFolder string
	innovationsFolder string
	tenetsFolder string
	culturesFolder string
	culturesBackupFolder string
	terrainFolder string
	metamodFolder string

	eventsFolder string
	onActionsFolder string

	processMap bool
	printEvents bool

	pixel_exponent float64
}

func readConfigFile(config string) configData {

	thisFile, err := os.Open(config)
	if err != nil {
		log.Fatal(err)
	}

	var configInfo configData
	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)

	for scanner.Scan() {
		// get next line
		line := scanner.Text()
		fields := strings.Split(line, ";")
		if len(fields) > 1 {
			if fields[0] == "map_data_folder" {
				configInfo.mapDataFolder = fields[1]
			} else if fields[0] == "landed_titles_folder" {
				configInfo.landedTitleFolder = fields[1]
			} else if fields[0] == "province_terrain_folder" {
				configInfo.terrainFolder = fields[1]
			} else if fields[0] == "cultures_folder" {
				configInfo.culturesFolder = fields[1]
			} else if fields[0] == "cultures_backup_folder" {
				configInfo.culturesBackupFolder = fields[1]
			} else if fields[0] == "innovations_folder" {
				configInfo.innovationsFolder = fields[1]
			} else if fields[0] == "doctrines_folder" {
				configInfo.tenetsFolder = fields[1]
			} else if fields[0] == "scripted_effects_out_folder" {
				configInfo.scriptedEffectsOutFolder = fields[1]
			} else if fields[0] == "metamod_folder" {
				configInfo.metamodFolder = fields[1]
			} else if fields[0] == "events_folder" {
				configInfo.eventsFolder = fields[1]
			} else if fields[0] == "on_actions_folder" {
				configInfo.onActionsFolder = fields[1]
			} else if fields[0] == "pixel_exponent" {
				configInfo.pixel_exponent, err = strconv.ParseFloat(fields[1], 64)
				if err != nil {
					fmt.Println("Error setting parameter \"pixel_exponent\". \"" +  fields[1] + "\" is not a valid float64.")
					log.Fatal(err)
				}
			} else if fields[0] == "process_map" {
				if fields[1] == "true" || fields[1] == "yes" {
					configInfo.processMap = true
				}
			} else if fields[0] == "print_events" {
				if fields[1] == "true" || fields[1] == "yes" {
					configInfo.printEvents = true
				}
			}
		}
	}
	// check for errors
	paramError := false
	if len(configInfo.mapDataFolder) < 1 {
		fmt.Println("Config File is missing parameter \"map_data_folder\"")
		paramError = true
	}
	if len(configInfo.landedTitleFolder) < 1 {
		fmt.Println("Config File is missing parameter \"landed_titles_folder\"")
		paramError = true
	}
	if len(configInfo.terrainFolder) < 1 {
		fmt.Println("Config File is missing parameter \"province_terrain_folder\"")
		paramError = true
	}
	if len(configInfo.culturesFolder) < 1 {
		fmt.Println("Config File is missing parameter \"cultures_folder\"")
		paramError = true
	}
	if len(configInfo.innovationsFolder) < 1 {
		fmt.Println("Config File is missing parameter \"innovations_folder\"")
		paramError = true
	}
	if len(configInfo.metamodFolder) < 1 {
		fmt.Println("Config File is missing parameter \"metamod_folder\"")
		paramError = true
	}
	if len(configInfo.tenetsFolder) < 1 {
		fmt.Println("Config File is missing parameter \"doctrines_folder\"")
		paramError = true
	}
	if len(configInfo.scriptedEffectsOutFolder) < 1 {
		fmt.Println("Config File is missing parameter \"scripted_effects_out_folder\"")
		paramError = true
	}
	if len(configInfo.eventsFolder) < 1 {
		fmt.Println("Config File is missing parameter \"events_folder\"")
		paramError = true
	}
	if len(configInfo.onActionsFolder) < 1 {
		fmt.Println("Config File is missing parameter \"on_actions_folder\"")
		paramError = true
	}
	if configInfo.pixel_exponent == 0.0 {
		fmt.Println("Config File is missing parameter \"pixel_exponent\". Setting to default value of 3.")
	}
	if paramError {
		err := "Config file is missing parameters"
		log.Fatal(err)
	}
	return configInfo
}
