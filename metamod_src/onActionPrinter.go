package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

func onActionPrinter(outDir string, outName string) {
	err := os.MkdirAll(filepath.Dir(filepath.Join(outDir,outName)), 0755)
	if err != nil {
		fmt.Println("Failed to create new directory: " + outDir)
		time.Sleep(100)
		log.Fatal(err)
	}
	outFile, err := os.Create(filepath.Join(outDir, outName))
	if err != nil {
		fmt.Println("Failed to create new file: " + filepath.Join(outDir, outName))
		time.Sleep(100)
		log.Fatal(err)
	}
	writeHeader(outFile)

	j := 1
	for i := 1; i <= 365; i++ {
		_, err = outFile.WriteString("yearly_global_pulse = {\n")
		_, err = outFile.WriteString("\tevents = {\n")
		if i == 1 {
			_, err = outFile.WriteString("\t\tdemd_meta_population.000" + strconv.Itoa(i) + "\n")
		} else if i < 10 {
			_, err = outFile.WriteString("\t\tdelay = { days = " + strconv.Itoa(i-1) + " }\n")
			_, err = outFile.WriteString("\t\tdemd_meta_population.000" + strconv.Itoa(i) + "\n")
		} else if i < 100 {
			_, err = outFile.WriteString("\t\tdelay = { days = " + strconv.Itoa(i-1) + " }\n")
			_, err = outFile.WriteString("\t\tdemd_meta_population.00" + strconv.Itoa(i) + "\n")
		} else {
			_, err = outFile.WriteString("\t\tdelay = { days = " + strconv.Itoa(i-1) + " }\n")
			_, err = outFile.WriteString("\t\tdemd_meta_population.0" + strconv.Itoa(i) + "\n")
		}
		if i < 361 {
			if j < 10 {
				_, err = outFile.WriteString("\t\tdemd_meta_population.100" + strconv.Itoa(j) + "\n")
			} else {
				_, err = outFile.WriteString("\t\tdemd_meta_population.10" + strconv.Itoa(j) + "\n")
			}
			j++
			if j > 30 {
				j = 1
			}
		}
		_, err = outFile.WriteString("\t}\n")
		_, err = outFile.WriteString("}\n")
	}

}

func eventPrinter(outDir string, outName string) {
	err := os.MkdirAll(filepath.Dir(filepath.Join(outDir,outName)), 0755)
	if err != nil {
		fmt.Println("Failed to create new directory: " + outDir)
		time.Sleep(100)
		log.Fatal(err)
	}
	outFile, err := os.Create(filepath.Join(outDir, outName))
	if err != nil {
		fmt.Println("Failed to create new file: " + filepath.Join(outDir, outName))
		time.Sleep(100)
		log.Fatal(err)
	}
	writeHeader(outFile)
	_, err = outFile.WriteString("namespace = demd_meta_population\n\n")

	for i := 1; i <= 365; i++ {
		if i < 10 {
			_, err = outFile.WriteString("demd_meta_population.000" + strconv.Itoa(i) + " = {\n")
		} else if i < 100 {
			_, err = outFile.WriteString("demd_meta_population.00" + strconv.Itoa(i) + " = {\n")
		} else {
			_, err = outFile.WriteString("demd_meta_population.0" + strconv.Itoa(i) + " = {\n")
		}
		_, err = outFile.WriteString("\thidden = yes\n")
		_, err = outFile.WriteString("\ttype = empty\n")
		_, err = outFile.WriteString("\timmediate = {\n")
		_, err = outFile.WriteString("\t\tannualPulse = { list = eligible_counties_group_y_" + strconv.Itoa(i) + " }\n")
		_, err = outFile.WriteString("\t}\n")
		_, err = outFile.WriteString("}\n")
	}

	for i := 1; i <= 30; i++ {
		if i < 10 {
			_, err = outFile.WriteString("demd_meta_population.100" + strconv.Itoa(i) + " = {\n")
		} else if i < 100 {
			_, err = outFile.WriteString("demd_meta_population.10" + strconv.Itoa(i) + " = {\n")
		} else {
			_, err = outFile.WriteString("demd_meta_population.1" + strconv.Itoa(i) + " = {\n")
		}
		_, err = outFile.WriteString("\thidden = yes\n")
		_, err = outFile.WriteString("\ttype = empty\n")
		_, err = outFile.WriteString("\timmediate = {\n")
		_, err = outFile.WriteString("\t\tmonthlyPulse = { list = eligible_counties_group_m_" + strconv.Itoa(i) + " }\n")
		_, err = outFile.WriteString("\t}\n")
		_, err = outFile.WriteString("}\n")
	}
}
