package main

import (
	"bufio"
	"fmt"
	"image/png"
	"io/ioutil"
	"log"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func provinceFunction(mapDataFolder string, provinceTerrainFile string, provinceWinterFile string, landedTitleFile string, cultureFile string, backupCultureFile string, outDir string, outName string, pixelExp float64) {
	fmt.Println("finding cultures")
	cultureList := getCultures(cultureFile, backupCultureFile)
	fmt.Println("found cultures = " + strconv.Itoa(len(cultureList)))

	fmt.Println("processing definitions.csv")
	provinces, color2provID := getProvinces(mapDataFolder)
	fmt.Println("processing provinces.png")
	getProvinceInfo(mapDataFolder, provinces, color2provID)
	fmt.Println("processing 00_landed_titles.txt")
	_ = getTitleInfo(provinces, landedTitleFile)
	fmt.Println("computing province positions and elevation")
	getAvgPos(provinces)
	fmt.Println("finding major river and strait crossings")
	getAdjacencies(provinces, mapDataFolder)
	fmt.Println("finding and classifying water provinces")
	getWater(provinces)
	fmt.Println("finding neighboring and nearby provinces")
	getNeighbors(provinces, mapDataFolder, color2provID)
	getNearby(provinces)
	fmt.Println("finding provinces on minor rivers")
	getMinorRivers(provinces, mapDataFolder)
	fmt.Println("finding provinces on major bodies of water")
	getMajorWaterAdjacency(provinces)
	fmt.Println("retrieve province terrains")
	getTerrainTypes(provinceTerrainFile, provinces)
	fmt.Println("finding ports")
	getPort(provinces)
	fmt.Println("retrieving winter data")
	getWinterData(provinceWinterFile,provinces)
	fmt.Println("finding arable area")
	getNumFarmDistricts(provinces)
	fmt.Println("converting province data to county data")
	counties := province2county(provinces)
	fmt.Println("get average county terrain")
	getCountyTerrainFracs(counties)
	fmt.Println("assign resources")
	resourceAssigner(counties)
	calculateSplitInsideCounty(counties)
	getCountySanitaton(counties)
	fmt.Println("writing to disk")
	writeProvinces(counties, provinces, cultureList, outDir, outName, pixelExp)
}

func writeProvinces(counties map[string]*county, provinces map[string]*prov, cultureList map[string]string, outDir string, outName string, pixelExp float64) {
	err := os.MkdirAll(filepath.Dir(filepath.Join(outDir,outName)), 0755)
	outFile, err := os.Create(filepath.Join(outDir, outName))
	if err != nil {
		fmt.Println("Failed to create new file: " + filepath.Join(outDir, outName))
		time.Sleep(100)
		log.Fatal(err)
	}
	fmt.Println("created file at " + filepath.Join(outDir, outName))

	writeHeader(outFile)
	_, _ = outFile.WriteString("initialize_province_variables = {\n")
	_, _ = outFile.WriteString("\n")

	counter := 1
	counterM := 1
	for _, province := range counties {
		if !(province.x == 0 && province.y == 0) && !math.IsNaN(province.x) && !math.IsNaN(province.y) { // try to weed out bad provinces from TFE
			// monthly split
			_, _ = outFile.WriteString("\tadd_to_global_variable_list = { name = eligible_counties_group_m_" + strconv.Itoa(counterM) + " target = title:" + province.name + " }\n")
			// annual split
			_, _ = outFile.WriteString("\tadd_to_global_variable_list = { name = eligible_counties_group_y_" + strconv.Itoa(counter) + " target = title:" + province.name + " }\n")
			// annual split
			_, _ = outFile.WriteString("\tadd_to_global_variable_list = { name = eligible_counties_all target = title:" + province.name + " }\n")
			counter++
			counterM++
			if counter > 365 {
				counter = 1
			}
			if counterM > 30 {
				counterM = 1
			}
		}
	}

	for culture := range cultureList {
		_, _ = outFile.WriteString("\tadd_to_global_variable_list = { name = culture_list target = culture:" + culture + " }\n")
	}
	_, _ = outFile.WriteString("\n")
	for _, province := range counties {
		_, _ = outFile.WriteString("\ttitle:" + province.name + " = {\n")
		_, _ = outFile.WriteString("\t\tset_variable = { name = id value = " + province.capitalID + " }\n")
		if !math.IsNaN(province.x) && !math.IsNaN(province.y) { // print placeholder vals if x&y are NaN. Shouldn't be necessary but when you work with Wrench...
			_, _ = outFile.WriteString("\t\tset_variable = { name = x value = " + fmt.Sprintf("%.3f", province.x) + " }\n")
			_, _ = outFile.WriteString("\t\tset_variable = { name = y value = " + fmt.Sprintf("%.3f", province.y) + " }\n")
		} else {
			_, _ = outFile.WriteString("\t\tset_variable = { name = x value = 0 }\n")
			_, _ = outFile.WriteString("\t\tset_variable = { name = y value = 0 }\n")
		}
		_, _ = outFile.WriteString("\t}\n\n")
	}

	for id, province := range provinces {
		if province.isValidForHolding {
			_, _ = outFile.WriteString("\tprovince:" + id + " = { # " + province.name + "\n")
			if province.isOnLake {
				_, _ = outFile.WriteString("\t\tset_variable = { name = isOnLake value = 1 }\n")
				_, _ = outFile.WriteString("\t\tcounty = { set_variable = { name = isOnLake value = 1 } }\n")
			}
			if province.isOnMajorRiver {
				_, _ = outFile.WriteString("\t\tset_variable = { name = isOnMajorRiver value = 1 }\n")
				_, _ = outFile.WriteString("\t\tcounty = { set_variable = { name = isOnMajorRiver value = 1 } }\n")
			}
			if province.isOnMinorRiver {
				_, _ = outFile.WriteString("\t\tset_variable = { name = isOnMinorRiver value = 1 }\n")
				_, _ = outFile.WriteString("\t\tcounty = { set_variable = { name = isOnMinorRiver value = 1 } }\n")
			}
			if province.isMajorRiverCrossing {
				_, _ = outFile.WriteString("\t\tset_variable = { name = isMajorRiverCrossing value = 1 }\n")
				_, _ = outFile.WriteString("\t\tcounty = { set_variable = { name = isMajorRiverCrossing value = 1 } }\n")
			}
			if province.isStraitCrossing {
				_, _ = outFile.WriteString("\t\tset_variable = { name = isStraitCrossing value = 1 }\n")
				_, _ = outFile.WriteString("\t\tcounty = { set_variable = { name = isStraitCrossing value = 1 } }\n")
			}
			_, _ = outFile.WriteString("\t\tset_variable = { name = winter_severity_bias value = " + fmt.Sprintf("%.2f", province.winterSeverityBias) + " }\n")
			_, _ = outFile.WriteString("\t\tset_variable = { name = sqrt_pixels value = " + fmt.Sprintf("%.2f", math.Pow(float64(len(province.pixels)), 1.0/pixelExp)) + " } # sqrt pixels " +
			 fmt.Sprintf("%.2f", math.Pow(float64(len(province.pixels)), 0.5)) + "\n")

			_, _ = outFile.WriteString("\t}\n\n")
		}
	}
	_, _ = outFile.WriteString("}")
}

func calculateSplitInsideCounty(counties map[string]*county) {
	for _, county := range counties {
		x := len(county.provIDs)

		county.defenseShareMinor = 0.1
		county.defenseShareMajor = 1.0 - float64(x) * county.defenseShareMinor
	}

}


type prov struct {
	name      string
	x         float64
	y         float64
	elevation float64
	terrain   string
	geoRegion string

	color     string
	ID        string
	neighbors []string
	nearby    []string
	port      string
	pixels    []*pixel

	baseWater     float64
	baseFertility float64

	isValidForHolding    bool
	isLand               bool
	isOnMinorRiver       bool
	isOnMajorRiver       bool
	isOnLake             bool
	isMajorRiverCrossing bool
	isStraitCrossing     bool
	isCoastal            bool

	isLake       bool
	isMajorRiver bool
	isSea        bool

	barony  string
	county  string
	duchy   string
	kingdom string
	empire  string

	isDuchyCapital  bool
	isCountyCapital bool

	duchyCapital  string
	countyCapital string

	duchyProvinces  []string
	countyProvinces []string

	pixelArea          float64
	farmDistrictBase   float64
	transportCostModifier   float64
	winterSeverityBias float64

	tradePower float64
}

type county struct {
	name string
	x    float64
	y    float64

	elevation float64
	latitude  float64

	geoRegion string

	capitalID string

	provIDs   []string // does not include capital
	neighbors []string

	terrains     []string
	terrainFracs map[string]float64

	coastalFrac    float64
	lakeFrac       float64
	minorRiverFrac float64
	majorRiverFrac float64
	resource       string
	resourceBonus float64
	resourceBonusMax float64

	terrainSanitation float64

	isDuchyCapital bool

	farmDistrictsBase float64
	fertilityBase     float64

	defenseShareMajor float64
	defenseShareMinor float64
}

func resourceAssigner(provinces map[string]*county) {
	for _, prov := range provinces {
		resourceWeights := make(map[string]float64)

		// wine
		designatedWine := []string{"c_bordeaux", "c_auxerre", "c_bresse", "c_sancerre", "c_alcacer_do_sal", "c_vienna", "c_anjou",
			"c_perigord", "c_siena", "c_monferrato", "c_turin", "c_lecce", "c_trier", "c_koblenz", "c_venaissin", "c_worms",
			"c_mainz", "c_kaiserslautern" }
		for _, county := range designatedWine {
			if prov.name == county {
				resourceWeights["wine"] = 1000
			}
		}

		// furs
		designatedFurs := []string{}
		for _, county := range designatedFurs {
			if prov.name == county {
				resourceWeights["furs"] = 1000
			}
		}

		// salt
		designatedSalt := []string{"c_sacz", "c_barcelona", "c_hallstatt", "c_salzburg", "c_aytos", "c_soli", "c_danakil",
			"c_edd", "c_afar", "c_worcestershire", "c_cheshire", "c_cleves", "c_berchtesgaden", "c_vaihingen", "c_ailech",
			"c_agrigento", "c_palermo", "c_ural"}
		for _, county := range designatedSalt {
			if prov.name == county {
				resourceWeights["salt"] = 1000
			}
		}

		// spices
		designatedSpices := []string{}
		for _, county := range designatedSpices {
			if prov.name == county {
				resourceWeights["spices"] = 1000
			}
		}

		// silk
		designatedSilk := []string{}
		for _, county := range designatedSilk {
			if prov.name == county {
				resourceWeights["silk"] = 1000
			}
		}

		// dyes
		designatedDyes := []string{}
		for _, county := range designatedDyes {
			if prov.name == county {
				resourceWeights["dyes"] = 1000
			}
		}

		// jewelry
		designatedPreciousMetals := []string{"c_innsbruck", "c_gottingen", "c_trenscen", "c_niani", "c_bure", "c_bambuk", "c_cagliari",
			"c_chalkidike", "c_nishapur", "c_pomoravlje", "c_zarand", "c_spis", "c_sijilmasa", "c_kaffa"} // add Trepca
		for _, county := range designatedPreciousMetals {
			if prov.name == county {
				resourceWeights["precious_metals"] = 1000
			}
		}

		resourceWeights["none"] = 1

		// get highest
		highestK := ""
		highestV := 0.0
		for k, v := range resourceWeights {
			if v > highestV {
				highestV = v
				highestK = k
			}
		}
		prov.resource = highestK

		if prov.resource == "furs" {
			prov.resourceBonus = 0.25
		} else if prov.resource == "precious_metals" {
			prov.resourceBonus = 1
		} else if prov.resource == "dyes" {
			prov.resourceBonus = 0.5
		} else if prov.resource == "silk" {
			prov.resourceBonus = 0.5
		} else if prov.resource == "spices" {
			prov.resourceBonus = 0.5
		} else if prov.resource == "wine" {
			prov.resourceBonus = 0.25
		}
		prov.resourceBonus += 1
	}
}

func getCountyTerrainFracs(provinces map[string]*county) {
	for _, prov := range provinces {
		terrainFracs := make(map[string]float64)
		terrainFracs["floodplains"] = 0.0; terrainFracs["farmlands"] = 0.0; terrainFracs["plains"] = 0.0; terrainFracs["forest"] = 0.0
		terrainFracs["oasis"] = 0.0; terrainFracs["taiga"] = 0.0; terrainFracs["hills"] = 0.0; terrainFracs["mountains"] = 0.0
		terrainFracs["wetlands"] = 0.0; terrainFracs["jungle"] = 0.0; terrainFracs["drylands"] = 0.0; terrainFracs["steppe"] = 0.0; terrainFracs["desert_mountains"] = 0.0
		terrainFracs["desert"] = 0.0

		fmt.Println("county = " + prov.name)
		for _, terrain := range prov.terrains {
			for k := range terrainFracs {
				if k == terrain {
					//fmt.Println("found terrain " + terrain)
					terrainFracs[terrain]++
				}
			}
		}
		for k := range terrainFracs {

			terrainFracs[k] = terrainFracs[k] / float64(len(prov.terrains))
			//fmt.Println("debug " + k, terrainFracs[k])
		}
		prov.terrainFracs = terrainFracs
	}
}



func getCountySanitaton(counties map[string]*county) {

	for _, thisCounty := range counties {
		if thisCounty.majorRiverFrac > 0 {
			thisCounty.terrainSanitation = 2.0
		} else if thisCounty.minorRiverFrac > 0 {
			thisCounty.terrainSanitation = 1.5
		} else if thisCounty.lakeFrac > 0 {
			thisCounty.terrainSanitation = 1.25
		} else if thisCounty.coastalFrac > 0 {
			thisCounty.terrainSanitation = 1.25
		} else {
			thisCounty.terrainSanitation = 1.0
		}
	}
}

func province2county(provinces map[string]*prov) map[string]*county {
	counties := make(map[string]*county)
	for _, province := range provinces {
		if province.isCountyCapital {
			var newCounty county
			newCounty.name = province.county
			newCounty.x = province.x
			newCounty.y = province.y
			newCounty.elevation = province.elevation
			newCounty.geoRegion = province.geoRegion
			newCounty.capitalID = province.ID
			newCounty.provIDs = province.countyProvinces
			newCounty.farmDistrictsBase = province.farmDistrictBase
			newCounty.fertilityBase = province.baseFertility
			for _, prv := range province.countyProvinces {
				newCounty.fertilityBase += provinces[prv].baseFertility
			}
			newCounty.fertilityBase = newCounty.fertilityBase / float64(len(province.countyProvinces)+1)


			newCounty.terrains = []string{}
			newCounty.isDuchyCapital = province.isDuchyCapital

			if province.isCoastal {
				newCounty.coastalFrac = 1
			} else {
				newCounty.coastalFrac = 0
			}
			for _, countyProvID := range newCounty.provIDs {
				if provinces[countyProvID].isCoastal {
					newCounty.coastalFrac++
				}
			}
			newCounty.coastalFrac = newCounty.coastalFrac / float64(len(newCounty.provIDs) + 1)

			if province.isOnLake {
				newCounty.lakeFrac = 1
			} else {
				newCounty.lakeFrac = 0
			}
			for _, countyProvID := range newCounty.provIDs {
				if provinces[countyProvID].isOnLake {
					newCounty.lakeFrac++
				}
			}
			newCounty.lakeFrac = newCounty.lakeFrac / float64(len(newCounty.provIDs) + 1)

			if province.isOnMinorRiver {
				newCounty.minorRiverFrac = 1
			} else {
				newCounty.minorRiverFrac = 0
			}
			for _, countyProvID := range newCounty.provIDs {
				if provinces[countyProvID].isOnMinorRiver {
					newCounty.minorRiverFrac++
				}
			}
			newCounty.minorRiverFrac = newCounty.minorRiverFrac / float64(len(newCounty.provIDs) + 1)

			if province.isOnMajorRiver {
				newCounty.majorRiverFrac = 1
			} else {
				newCounty.majorRiverFrac = 0
			}
			for _, countyProvID := range newCounty.provIDs {
				if provinces[countyProvID].isOnMajorRiver {
					newCounty.majorRiverFrac++
				}
			}
			newCounty.majorRiverFrac = newCounty.majorRiverFrac / float64(len(newCounty.provIDs) + 1)

			// sum up farming cap
			for _, countyProvID := range newCounty.provIDs {
				newCounty.farmDistrictsBase += provinces[countyProvID].farmDistrictBase
				newCounty.terrains = append(newCounty.terrains, provinces[countyProvID].terrain)
				//newCounty.fertilityBase += provinces[countyProvID].baseFertility
			}

			counties[newCounty.name] = &newCounty
		}

	}

	return counties

}

func getNumFarmDistricts(provinces map[string]*prov) {
	districtMult := make(map[string]float64)
	districtMult["floodplains"] = 1.2; districtMult["farmlands"] = 1.2; districtMult["oasis"] = 1.0; districtMult["plains"] = 1.0
	districtMult["drylands"] = 1.0; districtMult["forest"] = 0.9; districtMult["hills"] = 0.7; districtMult["wetlands"] = 0.5
	districtMult["jungle"] = 0.5; districtMult["taiga"] = 0.5; districtMult["steppe"] = 0.5
	districtMult["mountains"] = 0.5; districtMult["desert"] = 0.4; districtMult["desert_mountains"] = 0.3


	fertilityMult := make(map[string]float64)
	fertilityMult["floodplains"] = 1.5; fertilityMult["farmlands"] = 1.45; fertilityMult["oasis"] = 1.45; fertilityMult["plains"] = 1.4
	fertilityMult["forest"] = 1.35; fertilityMult["drylands"] = 1.3; fertilityMult["wetlands"] = 1.3; fertilityMult["jungle"] = 1.3
	fertilityMult["hills"] = 1.25; fertilityMult["mountains"] = 1.2; fertilityMult["steppe"] = 1.15; fertilityMult["taiga"] = 1.1
	fertilityMult["desert"] = 1.05; fertilityMult["desert_mountains"] = 1.0


	for _, prov := range provinces {

		// District Count
		mult := districtMult[prov.terrain]
		if prov.isCoastal {
			mult += 0.3
		} else if prov.isOnMajorRiver {
			mult += 0.15
		} else if prov.isOnMinorRiver {
			mult += 0.15
		} else if prov.isOnLake {
			mult += 0.15
		}
		prov.farmDistrictBase = 6.0 * mult * math.Pow(float64(len(prov.pixels)), 1/3)

		// Fertility
		mult = fertilityMult[prov.terrain]-1
		if prov.isOnMajorRiver {
			mult += 0.3
		} else if prov.isOnMinorRiver {
			mult += 0.3
		} else if prov.isOnLake {
			mult += 0.15
		} else if prov.isCoastal {
			mult += 0.15
		}
		mult += (1-prov.winterSeverityBias) * 0.2 // WSV is 0-1, so 2 - WSV is 0-1 as well



		prov.baseFertility = mult // 50% from terrain, 30% from water features, 20% from winter severity bias

		if prov.isCoastal {
			prov.transportCostModifier = -0.3
		} else if prov.isOnMajorRiver {
			prov.transportCostModifier = -0.25
		} else if prov.isOnMinorRiver {
			prov.transportCostModifier = -0.2
		}

		prov.tradePower = 1
		if prov.isCoastal && prov.isOnMajorRiver {
			prov.tradePower += 1
		} else if prov.isOnMajorRiver {
			prov.tradePower += 0.5
		} else if prov.isCoastal {
			prov.tradePower += 0.5
		} else if prov.isOnMinorRiver {
			prov.tradePower += 0.3
		}
	}
}

type title struct {
	name string
	tier int
	geoRegion string
	capitalProvince string
	capitalBarony string
	capitalCounty string
	titleAbove *title
	titleBelow []*title
	provinces []string
}

func getCleanedString(string string) string {
	cleanString := strings.ReplaceAll(string, "=", "")
	return cleanString
}

func getTitleInfo(provinces map[string]*prov, landedTitlesPath string) map[string]*title {
	// open file
	thisFile, err := os.Open(landedTitlesPath)
	if err != nil {
		fmt.Println("Failed to open landed titles file: " + landedTitlesPath)
		log.Fatal(err)
	}
	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)

	titles := make(map[string]*title)

	var currentTitleReal title
	currentTitleReal.tier = -1
	currentTitleReal.provinces = []string{}
	currentTitleReal.titleBelow = []*title{}
	currentEmpire := &currentTitleReal
	currentKingdom := &currentTitleReal
	currentDuchy := &currentTitleReal
	currentCounty := &currentTitleReal
	currentBarony := &currentTitleReal
	currentTitle := &currentTitleReal
	baroniesRead := 0
	for scanner.Scan() {
		// get next line
		line := scanner.Text()
		line = cleanLine(line)
		fields := strings.Fields(line)
		if len(fields) > 0 {
			identifier := strings.Split(fields[0], "_")[0]
			if identifier == "e" {
				var newEmpire title
				newEmpire.name = fields[0]
				newEmpire.tier = 4
				currentEmpire = &newEmpire
				currentTitle = &newEmpire
				titles[newEmpire.name] = &newEmpire
			} else if identifier == "k" {
				var newKingdom title
				newKingdom.name = fields[0]
				newKingdom.tier = 3
				newKingdom.titleAbove = currentEmpire
				currentEmpire.titleBelow = append(currentEmpire.titleBelow, &newKingdom)
				currentKingdom = &newKingdom
				currentTitle = &newKingdom
				titles[newKingdom.name] = &newKingdom
			} else if identifier == "d" {
				var newDuchy title
				newDuchy.name = fields[0]
				newDuchy.tier = 2
				newDuchy.titleAbove = currentKingdom
				currentKingdom.titleBelow = append(currentKingdom.titleBelow, &newDuchy)
				currentDuchy = &newDuchy
				currentTitle = &newDuchy
				titles[newDuchy.name] = &newDuchy
			} else if identifier == "c" {
				var newCounty title
				newCounty.name = getCleanedString(fields[0])
				newCounty.tier = 1
				newCounty.titleAbove = currentDuchy
				newCounty.capitalCounty = newCounty.name
				currentDuchy.titleBelow = append(currentDuchy.titleBelow, &newCounty)
				currentCounty = &newCounty
				currentTitle = &newCounty
				titles[newCounty.name] = &newCounty
				baroniesRead = 0
			} else if identifier == "b" {
				var newBarony title
				newBarony.name = fields[0]
				newBarony.tier = 0
				newBarony.titleAbove = currentCounty
				newBarony.capitalBarony = newBarony.name
				currentCounty.titleBelow = append(currentCounty.titleBelow, &newBarony)
				currentBarony = &newBarony
				currentTitle = &newBarony
				titles[newBarony.name] = &newBarony
				if baroniesRead == 0 {
					currentCounty.capitalBarony = newBarony.name
				}
				baroniesRead++
			} else if fields[0] == "province" && len(fields) > 2 && currentTitle.tier == 0 {
				currentBarony.capitalProvince = fields[2]
				provinces[fields[2]].barony = currentBarony.name
				provinces[fields[2]].county = currentCounty.name
				provinces[fields[2]].duchy = currentDuchy.name
				provinces[fields[2]].kingdom = currentKingdom.name
				provinces[fields[2]].empire = currentEmpire.name
				provinces[fields[2]].isValidForHolding = true
				currentBarony.provinces = append(currentBarony.provinces, fields[2])
				currentCounty.provinces = append(currentCounty.provinces, fields[2])
				currentDuchy.provinces = append(currentDuchy.provinces, fields[2])
				currentKingdom.provinces = append(currentKingdom.provinces, fields[2])
				currentEmpire.provinces = append(currentEmpire.provinces, fields[2])
			} else if fields[0] == "capital" && len(fields) > 2 && currentTitle.tier > 1 {
				currentTitle.capitalCounty = fields[2]
			}
		}
	}

	// get the capital of the title as a province id rather than a county name (e.g. 482 rather than c_rome)
	for _, title := range titles {
		if title.tier > 1 {
			// capital = c_whatever to capital = b_whatever
			if capitalCounty, ok := titles[title.capitalCounty]; ok {
				title.capitalBarony = capitalCounty.capitalBarony
			} else {
				fmt.Println("Failed to find capital county " + title.capitalCounty + " for title " + title.name)
			}
		}
		if title.tier > 0 {
			// capital = b_whatever to capital = prov id
			if capitalBarony, ok := titles[title.capitalBarony]; ok {
				title.capitalProvince = capitalBarony.capitalProvince
			} else {
				fmt.Println("Failed to find capital barony " + title.capitalBarony + " for county " + title.name)
			}
		}
	}

	// identify county and duchy capital provinces
	for _, title := range titles {
		if title.tier == 1 {
			if capProv, ok := provinces[title.capitalProvince]; ok {
				capProv.isCountyCapital = true
			}
		} else if title.tier == 2 {
			if capProv, ok := provinces[title.capitalProvince]; ok {
				capProv.isDuchyCapital = true
			}
		}
	}
	// attach lists of all other provinces in the duchy/county to every duchy/county capital
	for _, province := range provinces {
		if province.isValidForHolding {
			for _, provInDuchy := range titles[province.duchy].provinces {
				if provInDuchy != province.ID {
					province.duchyProvinces = append(province.duchyProvinces, provInDuchy)
				}
			}
			for _, provInCounty := range titles[province.county].provinces {
				if provInCounty != province.ID {
					province.countyProvinces = append(province.countyProvinces, provInCounty)
				}
			}
			province.duchyCapital = titles[province.duchy].capitalProvince
			province.countyCapital = titles[province.county].capitalProvince
		}
	}
	return titles
}



func getPort(provinces map[string]*prov) {
	for _, province := range provinces {
		province.port = ""
		if province.isOnMajorRiver || province.isCoastal {
			province.port = province.ID
		} else {
			nearestPortDistance := math.MaxFloat64
			for _, neighborID := range province.nearby {
				if provinces[neighborID].isOnMajorRiver || provinces[neighborID].isCoastal {
					if getDistance(province, provinces[neighborID]) < nearestPortDistance {
						nearestPortDistance = getDistance(province, provinces[neighborID])
						province.port = neighborID
					}
				}
			}
		}
	}
}

func getNearby(provinces map[string]*prov) {
	for _, provA := range provinces {
		for _, provB := range provinces {
			if provA.ID < provB.ID && provA.isValidForHolding && provB.isValidForHolding {
				if getDistance(provA, provB) < 100.0 {
					provA.nearby = append(provA.nearby, provB.ID)
					provB.nearby = append(provB.nearby, provA.ID)
				}
			}
		}
	}
}

func getDistance(provA *prov, provB *prov) float64 {
	return math.Sqrt(math.Pow(provA.x - provB.x, 2) + math.Pow(provA.y - provB.y, 2))
}

func getWinterData(file string, provinces map[string]*prov) {
	thisFile, err := os.Open(file)
	if err != nil {
		fmt.Println("Failed to open file: " + file)
		log.Fatal(err)
	}
	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)
	currentProv := ""
	isInsideProv := false
	for scanner.Scan() {
		// get next line

		line := cleanLine(scanner.Text())
		fields := strings.Fields(line)
		if len(fields) > 0 {
			if _, ok := provinces[fields[0]]; ok {
				currentProv = fields[0]
				isInsideProv = true
			} else if fields[0] == "winter_severity_bias" && isInsideProv && len(fields) > 2 {
				provinces[currentProv].winterSeverityBias, _ = strconv.ParseFloat(fields[2], 64)
			}
		}
	}
}

func getTerrainTypes(provinceTerrainFile string, provinces map[string]*prov) {
	thisFile, err := os.Open(provinceTerrainFile)
	if err != nil {
		fmt.Println("Failed to open file: " + provinceTerrainFile)
		log.Fatal(err)
	}
	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)

	for scanner.Scan() {
		// get next line

		line := cleanLine(scanner.Text())
		fields := strings.Split(line, "=")
		if len(fields) > 1 {
			if _, ok := provinces[fields[0]]; ok && provinces[fields[0]].isLand {
				provinces[fields[0]].terrain = fields[1]
			}
		}

	}
}

func getMajorWaterAdjacency(provinces map[string]*prov) {
	for _, province := range provinces {
		if province.isLand {
			for _, neighborID := range province.neighbors {
				if provinces[neighborID].isMajorRiver {
					province.isOnMajorRiver = true
				}
				if provinces[neighborID].isLake {
					province.isOnLake = true
				}
				if provinces[neighborID].isSea {
					province.isCoastal = true
				}
			}
		}
	}
}

func getWater(provinces map[string]*prov) {
	for _, province := range provinces {
		fractionLand := 0.0
		for _, pixel := range province.pixels {
			if pixel.class == "land" {
				fractionLand++
			}
		}
		fractionLand = fractionLand / float64(len(province.pixels))
		if fractionLand > 0.5 {
			province.isLand = true
		}
	}

	for _, province := range provinces {
		if !province.isLand {
			riverIdentifiers := []string{"river", "ilinalta", "honrich", "geir", "yorgrim"}
			lakeIdentifiers := []string{"lakes", "lake", "lagoon", "lagoons"}
			if nameIndicatesType(province.name, riverIdentifiers) {
				province.isMajorRiver = true
			} else if nameIndicatesType(province.name, lakeIdentifiers) {
				province.isLake = true
			} else {
				province.isSea = true
			}
		}
	}
}

func nameIndicatesType(name string, identifiers []string) bool {
	fields := strings.Fields(name)
	for _, field := range fields {
		for _, identifier := range identifiers {
			if strings.EqualFold(field, identifier) {
				return true
			}
		}
	}
	substrings := strings.Split(name, "_")
	for _, substring := range substrings {
		for _, identifier := range identifiers {
			if strings.EqualFold(substring, identifier) {
				return true
			}
		}
	}
	return false
}



func getMinorRivers(provinces map[string]*prov, mapDataFolder string) {
	// Open Province File
	riverFile, err := os.Open(filepath.Join(mapDataFolder, "rivers.png"))
	if err != nil {
		log.Fatal(err)
	}

	riverMap, err := png.Decode(riverFile)
	if err != nil {
		log.Fatal(err)
	}

	for _, province := range provinces {
		for _, pix := range province.pixels {
			if pix.class == "river" {
				province.isOnMinorRiver = true
				break
			} else {
				neighborPixels := [][]int{{int(pix.x) + 1, int(pix.y) + 1}, {int(pix.x) - 1, int(pix.y) + 1}, {int(pix.x) + 1, int(pix.y) - 1}, {int(pix.x) - 1, int(pix.y) - 1}}
				for _, neighbor := range neighborPixels {
					r, g, b, _ := riverMap.At(neighbor[0], neighbor[1]).RGBA()
					unifiedColor := rgbaToUnified(int(r / 0x101), int(g / 0x101) , int(b / 0x101))
					if unifiedColor != "255.255.255" && unifiedColor != "255.0.128" {
						province.isOnMinorRiver = true
						break
					}
				}
			}
		}
	}
}

func getNeighbors(provinces map[string]*prov, mapDataFolder string, color2provID map[string]string) {
	// Open Province File
	provinceFile, err := os.Open(filepath.Join(mapDataFolder, "provinces.png"))
	if err != nil {
		log.Fatal(err)
	}

	provinceMap, err := png.Decode(provinceFile)
	if err != nil {
		log.Fatal(err)
	}

	for _, province := range provinces {
		if province.isValidForHolding {
			for _, pix := range province.pixels {
				r, g, b, _ := provinceMap.At(int(pix.x), int(pix.y)).RGBA()
				unifiedColor := rgbaToUnified(int(r/0x101), int(g/0x101), int(b/0x101))

				neighborPixels := [][]int{{int(pix.x) + 1, int(pix.y) + 1}, {int(pix.x) - 1, int(pix.y) + 1}, {int(pix.x) + 1, int(pix.y) - 1}, {int(pix.x) - 1, int(pix.y) - 1}}
				for _, neighbor := range neighborPixels {
					r, g, b, _ := provinceMap.At(neighbor[0], neighbor[1]).RGBA()
					neighborColor := rgbaToUnified(int(r/0x101), int(g/0x101), int(b/0x101))
					if neighborColor != unifiedColor /*&& provinces[color2provID[neighborColor]].isValidForHolding*/ {
						makeNeighbors(province, provinces[color2provID[neighborColor]])
					}
				}
			}
		}
	}
}

func getAdjacencies(provinces map[string]*prov, mapDataFolder string) {
	thisFile, err := os.Open(filepath.Join(mapDataFolder, "adjacencies.csv"))
	if err != nil {
		fmt.Println("Failed to open file: " + filepath.Join(mapDataFolder, "adjacencies.csv"))
		log.Fatal(err)
	}
	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)

	for scanner.Scan() {
		// get next line
		line := scanner.Text()
		fields := strings.Split(line, ";")
		if len(fields) > 2 {
			if fields[2] == "river_large" {
				provinces[fields[0]].isMajorRiverCrossing = true
				provinces[fields[1]].isMajorRiverCrossing = true
			} else if fields[2] == "sea" {
				provinces[fields[0]].isStraitCrossing = true
				provinces[fields[1]].isStraitCrossing = true
			}
			makeNeighbors(provinces[fields[0]], provinces[fields[1]])
		}

	}
}

func makeNeighbors(prov1 *prov, prov2 *prov) {
	if prov1 == nil || prov2 == nil {
		return
	}
	for _, v := range prov1.neighbors {
		if v == prov2.ID {
			return
		}
	}
	prov1.neighbors = append(prov1.neighbors, prov2.ID)
	prov2.neighbors = append(prov2.neighbors, prov1.ID)
}

func getAvgPos(provinces map[string]*prov) {
	for _, prov := range provinces {
		for _, pix := range prov.pixels {
			prov.x += pix.x
			prov.y += pix.y
			prov.elevation += pix.elevation
		}
		prov.x = prov.x / float64(len(prov.pixels))
		prov.y = prov.y / float64(len(prov.pixels))
		prov.elevation = prov.elevation / float64(len(prov.pixels))
	}
}


func getProvinces(mapDataFolder string) (map[string]*prov, map[string]string) {
	thisFile, err := os.Open(filepath.Join(mapDataFolder, "definition.csv"))
	if err != nil {
		log.Fatal(err)
	}

	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)

	provinces := make(map[string]*prov)
	color2provID := make(map[string]string)

	for scanner.Scan() {
		// get next line
		line := scanner.Text()
		fields := strings.Split(line, ";")
		if len(fields) > 3 {
			color := fields[1] + "." + fields[2] + "." + fields[3]

			var newProv prov
			newProv.ID = fields[0]
			newProv.name = fields[4]
			newProv.color = color
			newProv.pixels = []*pixel{}

			provinces[newProv.ID] = &newProv
			color2provID[color] = newProv.ID
		}
	}
	return provinces, color2provID
}

func getProvinceInfo(mapDataFolder string, provinces map[string]*prov, color2provID map[string]string) map[string]*prov {
	// Open Province File
	provinceFile, err := os.Open(filepath.Join(mapDataFolder, "provinces.png"))
	if err != nil {
		log.Fatal(err)
	}

	provinceMap, err := png.Decode(provinceFile)
	if err != nil {
		log.Fatal(err)
	}

	// Open River File
	riverFile, err := os.Open(filepath.Join(mapDataFolder, "rivers.png"))
	if err != nil {
		log.Fatal(err)
	}

	riverMap, err := png.Decode(riverFile)
	if err != nil {
		log.Fatal(err)
	}

	// Open HeightMap File
	heightFile, err := os.Open(filepath.Join(mapDataFolder, "heightmap.png"))
	if err != nil {
		log.Fatal(err)
	}

	heightMap, err := png.Decode(heightFile)
	if err != nil {
		log.Fatal(err)
	}

	for y := provinceMap.Bounds().Min.Y; y < provinceMap.Bounds().Max.Y; y++ {
		for x := provinceMap.Bounds().Min.X; x < provinceMap.Bounds().Max.X; x++ {
			r, g, b, _ := provinceMap.At(x, y).RGBA()
			unifiedColor := rgbaToUnified(int(r / 0x101), int(g / 0x101) , int(b / 0x101))
			//fmt.Println(unifiedColor)
			provID := color2provID[unifiedColor]


			var pix pixel
			pix.x = float64(x)
			pix.y = float64(y)
			pix.color = unifiedColor
			pix.provID = provID

			// rivermap info
			rr, rg, rb, _ := riverMap.At(x, y).RGBA()
			unifiedColor = rgbaToUnified(int(rr / 0x101), int(rg / 0x101) , int(rb / 0x101))

			if unifiedColor == "255.255.255" {
				pix.class = "land"
			} else if unifiedColor == "255.0.128" {
				pix.class = "sea"
			} else {
				pix.class = "river"
			}

			// heightmap info
			hr, hg, hb, _ := heightMap.At(x, y).RGBA()
			pix.elevation = rgbToElevation(int(hr / 0x101), int(hg / 0x101), int(hb / 0x101))

			if _, ok := provinces[provID]; ok {
				provinces[provID].pixels = append(provinces[provID].pixels, &pix)
			} else {
				fmt.Println("trying to add pixel to province \"" + provID + "\", which does not exist.\nThis error occurs when the pixel color was not found in map_data\\definition.csv.")
			}
		}
	}
	return provinces
}



type pixel struct {
	color string
	provID string
	elevation float64
	class string
	x float64
	y float64
}

func rgbaToUnified(r int, g int, b int) string {
	newr := float64(r)
	newg := float64(g)
	newb := float64(b)
	return strconv.Itoa(int(newr)) + "." + strconv.Itoa(int(newg)) + "." + strconv.Itoa(int(newb))
}

func rgbToElevation(r int, g int, b int) float64 {

	sum := (float64(r) + float64(g) + float64(b)) / float64(3)

	x1 := 19.546
	x2 := 70.043
	y1 := -6.6
	y2 := 1883.0

	slope := (y2 - y1) / (x2 - x1)
	intercept := (-1.0) * slope * x1

	return sum * slope + intercept

}

func getCultures(path string, backupPath string) map[string]string {

	cultureList := make(map[string]string)

	if len(backupPath) > 0 {
		fileInfo, err := ioutil.ReadDir(backupPath)
		if err != nil {
			fmt.Println("failed to read directory: " + backupPath)
			log.Fatal(err)
		}

		for i := 0; i < len(fileInfo); i++ {
			//fmt.Println(strings.Split(fileInfo[i].Name(),".")[1])
			if strings.Split(fileInfo[i].Name(),".")[1] == "txt" {
				cultures := getCulture(filepath.Join(backupPath, fileInfo[i].Name()))
				for culture := range cultures {
					cultureList[culture] = ""
				}
			}
		}
	}

	fileInfo, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Println("failed to read directory: " + path)
		log.Fatal(err)
	}

	for i := 0; i < len(fileInfo); i++ {
		//fmt.Println(strings.Split(fileInfo[i].Name(),".")[1])
		if strings.Split(fileInfo[i].Name(),".")[1] == "txt" {
			cultures := getCulture(filepath.Join(path, fileInfo[i].Name()))
			for culture := range cultures {
				cultureList[culture] = ""
			}
		}
	}

	return cultureList
}

func getCulture(path string) map[string]string {
	cultures := make(map[string]string)

	input, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalln(err)
	}
	lines := strings.Split(string(input), "\n")

	for _, line := range lines {
		//fmt.Println(strconv.Itoa(getIndentLevel(line)) + " " + line)
		if getIndentLevel(line) == 4 && validCulture(line) && len(strings.Fields(line)) > 0 {
			cultures[strings.Fields(line)[0]] = ""
		}
	}
	return cultures
}

func validCulture(target string) bool {
	invalidStrings := []string{"graphical_cultures", "ethnicities", "mercenary_names", "}", "dynasty_of_location", "name_chance", "dynasty_names"}

	for _, a := range invalidStrings {
		if strings.Contains(target, a) {
			return false
		}
	}
	if len(strings.Fields(target)) > 0 && strings.Contains(strings.Fields(target)[0], "#") {
		return false
	}
	return true
}
