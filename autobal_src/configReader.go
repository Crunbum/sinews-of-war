
package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type configData struct {
	terrainTypesFolder string
	terrainFolder string
	testFile string
	genericsFile string
	genericWeights map[string]float64
	winterWeights  map[string]float64
}


func readConfigFile(config string) configData {

	thisFile, err := os.Open(config)
	if err != nil {
		log.Fatal(err)
	}

	genericWeights := make(map[string]float64)
	winterWeights := make(map[string]float64)

	var configInfo configData
	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)
	for scanner.Scan() {
		// get next line
		line := scanner.Text()
		fields := strings.Split(line, ";")
		if len(fields) > 1 {
			if fields[0] == "terrain_types_folder" {
				configInfo.terrainTypesFolder = fields[1]
			} else if fields[0] == "province_terrain_folder" {
				configInfo.terrainFolder = fields[1]
			} else if fields[0] == "test_file" {
				configInfo.testFile = fields[1]
			} else if fields[0] == "generics_file" {
				configInfo.genericsFile = fields[1]
			} else if fields[0] == "generic_weights" {
				for i := 1; i < len(fields); i++ {
					subFields := strings.Split(fields[i], ",")
					if len(subFields) < 2 {
						fmt.Println("Warning: improper syntax in config file for generic unit " + fields[i] + ". Proper syntax is \"name,freq\".")
					} else {
						freq, err := strconv.ParseFloat(subFields[1], 64)
						if err != nil {
							fmt.Println("Warning: failed to parse float in config file for generic unit " + fields[i])
						}
						genericWeights[subFields[0]] = freq
					}
				}
			} else if fields[0] == "winter_weights" {
				for i := 1; i < len(fields); i++ {
					subFields := strings.Split(fields[i], ",")
					if len(subFields) < 2 {
						fmt.Println("Warning: improper syntax in config file for winter " + fields[i] + ". Proper syntax is \"name,freq\".")
					} else {
						freq, err := strconv.ParseFloat(subFields[1], 64)
						if err != nil {
							fmt.Println("Warning: failed to parse float in config file for winter " + fields[i])
						}
						winterWeights[subFields[0]] = freq
					}
				}
			}
		}
	}

	sum := 0.0
	for _, weight := range genericWeights {
		sum += weight
	}
	for i, weight := range genericWeights {
		genericWeights[i] = weight/sum
	}

	sum = 0.0
	for _, weight := range winterWeights {
		sum += weight
	}
	for i, weight := range winterWeights {
		winterWeights[i] = weight/sum
	}

	configInfo.genericWeights = genericWeights
	configInfo.winterWeights = winterWeights

	// check for errors
	paramError := false
	if len(configInfo.terrainTypesFolder) < 1 {
		fmt.Println("Config File is missing parameter \"terrain_types_folder\"")
		paramError = true
	}
	if len(configInfo.terrainFolder) < 1 {
		fmt.Println("Config File is missing parameter \"province_terrain_folder\"")
		paramError = true
	}
	if len(configInfo.testFile) < 1 {
		fmt.Println("Config File is missing parameter \"test_file\"")
		paramError = true
	}
	if len(configInfo.genericsFile) < 1 {
		fmt.Println("Config File is missing parameter \"generics_file\"")
		paramError = true
	}
	if len(configInfo.genericWeights) < 1 {
		fmt.Println("Config File is missing parameter \"generic_weights\"")
		paramError = true
	}
	if len(configInfo.winterWeights) < 1 {
		fmt.Println("Config File is missing parameter \"winter_weights\"")
		paramError = true
	}
	if paramError {
		err := "Config file is missing parameters"
		log.Fatal(err)
	}
	return configInfo
}

