package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func readFile(file string, terrains map[string]*terrain) []unit {
	// open file from cmd line args

	thisFile, err := os.Open(file)
	if err != nil {
		fmt.Println("Failed to open file: " + file)
		log.Fatal(err)
	}
	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)

	// create first unit
	var currentUnit unit
	currentUnit.name = "If you see this, something is wrong"
	currentUnit.terrain = make(map[string][]float64)
	currentUnit.counters = make(map[string]float64)
	winter := make(map[string][]float64)
	winter["no_winter"] = []float64{0,0,0,0}
	winter["mild_winter"] = []float64{0,0,0,0}
	winter["harsh_winter"] = []float64{0,0,0,0}
	currentUnit.winter = winter


	units := make([]unit,0)

	isInsideCounterBlock := false
	isInsideTerrainBlock := false
	isInsideWinterBlock := false
	lastLine := ""
	numUnits := 0
	for scanner.Scan() {
		// get next line
		line := scanner.Text()
		fields := strings.Fields(line)

		if strings.Contains(lastLine, "counters = {") {
			isInsideCounterBlock = true
		} else if strings.Contains(line, "}") {
			isInsideCounterBlock = false
		}

		if strings.Contains(lastLine, "terrain_bonus = {") {
			isInsideTerrainBlock = true
		} else if strings.Contains(line, "}") {
			isInsideTerrainBlock = false
		}

		if strings.Contains(lastLine, "winter_bonus = {") {
			isInsideWinterBlock = true
		} else if strings.Contains(line, "}") {
			isInsideWinterBlock = false
		}


		if len(fields) > 0 {
			if fields[0] == "###" {
				if numUnits > 0 {
					//fmt.Println("saving unit " + currentUnit.name)
					units = append(units, currentUnit)
				}
				numUnits++

				// Going to next line
				scanner.Scan()
				nextLine := scanner.Text()
				fields := strings.Fields(nextLine)
				// getting name
				if len(fields) < 1 {
					log.Fatal("Blank line after ### in file " + file)
				}

				name := fields[0]
				//fmt.Println(name)

				// Creating new unit
				currentUnit = newUnit(name, terrains)

				// we want to make sure all units have values for these
				setWinter := make(map[string][]float64)
				setWinter["no_winter"] = []float64{0,0,0,0}
				setWinter["normal_winter"] = []float64{0,0,0,0}
				setWinter["harsh_winter"] = []float64{0,0,0,0}

				currentUnit.winter = setWinter

			} else {
				interpretLine(fields, &currentUnit, isInsideCounterBlock, isInsideTerrainBlock, isInsideWinterBlock)
			}
		}
		lastLine = line
	}



	//fmt.Println("saving unit " + currentUnit.name)
	//fmt.Println("hell0")
	//fmt.Println(currentUnit.winter)
	//time.Sleep(100)
	units = append(units, currentUnit)
	//fmt.Println("FIRST UNIT = " +  units[0].name)
	//debug(units)

	thisFile.Close()
	return units
}

// parse a line and save relevant data to the current unit data structure
func interpretLine(fields []string, currentUnit *unit, isInsideCounterBlock bool, isInsideTerrainBlock bool, isInsideWinterBlock bool) {
	//fmt.Println(fields)
	if fields[0] == "type" {
		if len(fields) > 2 {
			currentUnit.class = fields[2]
		}
	} else if fields[0] == "damage" {
		if len(fields) > 2 {
			currentUnit.damage, _ = strconv.ParseFloat(fields[2], 64)
		}
	} else if fields[0] == "toughness" {
		if len(fields) > 2 {
			currentUnit.toughness, _ = strconv.ParseFloat(fields[2], 64)
		}
	} else if fields[0] == "pursuit" {
		if len(fields) > 2 {
			currentUnit.pursuit, _ = strconv.ParseFloat(fields[2], 64)
		}
	} else if fields[0] == "screen" {
		if len(fields) > 2 {
			currentUnit.screen, _ = strconv.ParseFloat(fields[2], 64)
		}
	} else if fields[0] == "siege_value" {
		if len(fields) > 2 {
			currentUnit.siege_value, _ = strconv.ParseFloat(fields[2], 64)
		}
	} else if fields[0] == "siege_tier" {
		if len(fields) > 2 {
			currentUnit.siege_tier, _ = strconv.ParseFloat(fields[2], 64)
		}
	} else if fields[0] == "buy_cost" {
		if len(fields) > 5 {
			currentUnit.gold, _ = strconv.ParseFloat(fields[5], 64)
		}
	} else if fields[0] == "stack" {

		if len(fields) > 2 {
			currentUnit.regimentSize, _ = strconv.ParseFloat(fields[2], 64)

		}
	} else if fields[0] == "ai_quality" {
		if len(fields) > 5 {
			currentUnit.aiQuality, _ = strconv.ParseFloat(fields[5], 64)

		}
	} else if fields[0] == "holy_order_fallback" {
		if len(fields) > 2 {
			if fields[2] == "yes" {
				currentUnit.holyOrderFallback = true
			}
		}
	} else if fields[0] == "mercenary_fallback" {
		if len(fields) > 2 {
			if fields[2] == "yes" {
				currentUnit.mercenaryFallback = true
			}
		}
	} else if fields[0] == "can_recruit" {
		if len(fields) > 2 {
			if fields[2] == "no" {
				currentUnit.canRecruit = false
			}
		}
	} else if fields[0] == "icon" {
		if len(fields) > 2 {
			currentUnit.icon = fields[2]
		}
	} else if fields[0] == "#DEMD" {
		if len(fields) > 1 {
			if fields[1] == "noble" {
				currentUnit.isNoble = true
			} else if fields[1] == "cultural" {
				currentUnit.isCultural = true
			} else if fields[1] == "advanced" {
				currentUnit.isAdvanced = true
			} else if fields[1] == "levy" {
				currentUnit.isLevy = true
			} else {
				log.Fatal("invalid #DEMD flag" + fields[1] + " detected on unit " + currentUnit.name + ". Valid flags are \"noble\", \"cultural\", \"advanced\"")
			}

		}
	} else {
		if isInsideTerrainBlock {
			currentUnit.terrain[fields[0]] = terrainBonus(fields)
		} else if isInsideWinterBlock {
			currentUnit.winter[fields[0]] = terrainBonus(fields)
		} else if isInsideCounterBlock {
			if len(fields) > 2 {
				counterSize, err := strconv.ParseFloat(fields[2], 64)
				if err != nil {
					log.Fatal("bad counter information for unit " + currentUnit.name)
				}
				currentUnit.counters[fields[0]] = counterSize
			}
		}
	}
}

// calculate value of a terrain bonus from a line containing terrain bonus information
func terrainBonus(fields []string) []float64 {
	bonus := []float64{0.0, 0.0, 0.0, 0.0}
	current := "placeholder"
	possible := []string{"damage", "toughness", "screen", "pursuit"}
	for _, field := range fields {
		for _, item := range possible {
			if field == item {
				current = field
			}
		}
		val, err := strconv.ParseFloat(field, 64)
		if err == nil {
			if current == "damage" {
				bonus[0] += val
			} else if current == "toughness" {
				bonus[1] += val
			} else if current == "pursuit" {
				bonus[2] += val
			} else if current == "screen" {
				bonus[3] += val
			}

		}
	}
	return bonus
}