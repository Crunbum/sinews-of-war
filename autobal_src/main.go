package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

// global vars: combat
var goldBudget = 10000.0
var numberRegiments = 100.0
var maxTicks = 1000
var marginOfError = 1.0

// these must be set correctly to what is in common/defines!
var damageScalingFactor = 0.03
var hardCasualtyConversionRatio = 0.3
var basePursuitDamage = 0.01
var pursuitDays = 3
var minPursuitDamage = 0.01
var pursuitStatToDamageCoeff = 0.5
var ratioForMaxCounter = 2.0
var maxCounter = 0.9

func main() {
	fmt.Println()
	fmt.Println("#################################################")
	fmt.Println("Sinews of War Regiment Tools v.4.0")
	fmt.Println("By Vertimnus (NaniteSystems)")
	currentTime := time.Now()
	fmt.Println("Compiled " + currentTime.Month().String() + " " + strconv.Itoa(currentTime.Day()) + ", " + strconv.Itoa(currentTime.Year()))
	fmt.Println("#################################################")
	time.Sleep(100)

	if len(os.Args) < 2 {
		help()
	} else {

		configData := readConfigFile(os.Args[1])
		terrains := getAllTerrainTypes(configData.terrainTypesFolder)
		getTerrainFreqs(filepath.Join(configData.terrainFolder,"00_province_terrain.txt"), terrains)

		// receive locations of files to parse from command line
		/*mode := os.Args[1]*/

		testFile := filepath.Join(configData.testFile)
		genericsFile := filepath.Join(configData.genericsFile)
		autoBalance(testFile, genericsFile, terrains,"single", configData, true)
	}
}






// calculate gold ratios for test unit set against generic set
func autoBalance(testFile string, genericsFile string, terrains map[string]*terrain,outName string, configInfo configData,verbose bool) []unit {
	// make log file
	testUnits := readFile(testFile, terrains)
	genericUnits := readFile(genericsFile, terrains)
	return runTests(testUnits, genericUnits,terrains, outName, configInfo,verbose)

}


func runTests(testUnits []unit, genericUnits []unit, terrains map[string]*terrain, outName string, configInfo configData, verbose bool) []unit {
	currentDir, _ := os.Getwd()

	logPath := filepath.Join(currentDir, "autobal_" + outName + ".log")
	// fmt.Println(thisPath)
	logFile, err := os.Create(logPath)
	if err != nil {
		fmt.Println("Failed to create new file: " + logPath)
		log.Fatal(err)
	}
	// write header
	_, err = logFile.WriteString("CK3 Autobalancer 2: Log File" + "\n")

	// make out file
	outPath := filepath.Join(currentDir, "autobal_" + outName + ".out")
	// fmt.Println(thisPath)
	outFile, err := os.Create(outPath)
	if err != nil {
		fmt.Println("Failed to create new file: " + outPath)
		log.Fatal(err)
	}
	// write header
	_, err = outFile.WriteString("CK3 Autobalancer 2: Summary File" + "\n")
	// make out file
	sumPath := filepath.Join(currentDir, "autobal_" + outName + ".sum")
	// fmt.Println(thisPath)
	sumFile, err := os.Create(sumPath)
	if err != nil {
		fmt.Println("Failed to create new file: " + sumPath)
		log.Fatal(err)
	}
	// write header
	_, err = sumFile.WriteString("CK3 Autobalancer 2: Summary File" + "\n")


	if verbose { fmt.Println("\nReading generic units file...") }

	checkGenerics(genericUnits, configInfo.genericWeights)

	if verbose { fmt.Println("Beginning Test Battles...") }
	start := time.Now()

	geoAvgGoldRatio := 1.0
	avgGoldRatio := 0.0
	killMargin := 0.0
	goldMargin := 0.0
	if verbose { fmt.Println() }
	for i, testUnit := range testUnits {
		//fmt.Println("## Evaluating test unit " + strconv.Itoa(i+1) + " of " + strconv.Itoa(len(testUnits)) + "...")

		_, _ = logFile.WriteString("#################################################\n")
		_, _ = logFile.WriteString("### Test Unit = " + testUnit.name + "\n")
		_, _ = logFile.WriteString("#################################################\n")
		_, _ = outFile.WriteString("#################################################\n")
		_, _ = outFile.WriteString("### Test Unit = " + testUnit.name + "\n")
		_, _ = outFile.WriteString("#################################################\n")



		genericTotalGoldDamageDealt := 0.0
		genericTotalGoldDamageTaken := 0.0
		genericTotalLossesDealt := 0.0
		genericTotalLossesTaken := 0.0
		genericBRs := make(map[string]map[string]battleReport)
		for _, genericUnit := range genericUnits {

			_, _ = logFile.WriteString("#################################################\n")
			_, _ = logFile.WriteString("Generic Unit = " + genericUnit.name + "\n")
			_, _ = outFile.WriteString("Generic Unit = " + genericUnit.name + "\n")

			genericBRs[genericUnit.name] = allTerrainBattles(testUnit, genericUnit, terrains, configInfo, outFile, logFile)
			terrainWeightedGoldRatio, terrainWeightedKillRatio := computeTerrainWeightedRatios(genericBRs[genericUnit.name], terrains, configInfo)

			_, _ = logFile.WriteString("# Terrain Weighted Gold Ratio = " + fmt.Sprintf("%.2f", terrainWeightedGoldRatio) + "\n")
			_, _ = outFile.WriteString("# Terrain Weighted Gold Ratio = " + fmt.Sprintf("%.2f", terrainWeightedGoldRatio) + "\n")
			_, _ = logFile.WriteString("# Terrain Weighted Kill Ratio = " + fmt.Sprintf("%.2f", terrainWeightedKillRatio) + "\n")
			_, _ = outFile.WriteString("# Terrain Weighted Kill Ratio = " + fmt.Sprintf("%.2f", terrainWeightedKillRatio) + "\n")

			terrainTotalGoldDamageDealt, terrainTotalGoldDamageTaken, terrainTotalLossesDealt, terrainTotalLossesTaken := computeTerrainWeightedLosses(genericBRs[genericUnit.name], terrains, configInfo)
			genericTotalGoldDamageDealt += terrainTotalGoldDamageDealt * configInfo.genericWeights[genericUnit.name]
			genericTotalGoldDamageTaken += terrainTotalGoldDamageTaken * configInfo.genericWeights[genericUnit.name]
			genericTotalLossesDealt += terrainTotalLossesDealt * configInfo.genericWeights[genericUnit.name]
			genericTotalLossesTaken += terrainTotalLossesTaken * configInfo.genericWeights[genericUnit.name]
		}
		genericTotalKillRatio := genericTotalLossesDealt / genericTotalLossesTaken
		genericTotalKillMargin := genericTotalLossesDealt - genericTotalLossesTaken
		genericTotalGoldRatio := genericTotalGoldDamageDealt / genericTotalGoldDamageTaken
		testUnit.finalGoldRatio = genericTotalGoldRatio
		// debug
		fmt.Println(testUnit.name)
		fmt.Println("GR: " + fmt.Sprintf("%.2f", testUnit.finalGoldRatio))
		fmt.Println("KR: " + fmt.Sprintf("%.2f", genericTotalKillRatio))
		for _, genericUnit := range genericUnits {
			terrainTotalGoldDamageDealt, terrainTotalGoldDamageTaken, terrainTotalLossesDealt, terrainTotalLossesTaken := computeTerrainWeightedLosses(genericBRs[genericUnit.name], terrains, configInfo)
			fmt.Println("vs " + genericUnit.name + ",GKR: " + fmt.Sprintf("%.2f", terrainTotalGoldDamageDealt/terrainTotalGoldDamageTaken)+ ",KR: " + fmt.Sprintf("%.2f", terrainTotalLossesDealt/terrainTotalLossesTaken) )

		}
		fmt.Println("\n\n")
		testUnits[i] = testUnit


		//fmt.Println("debug 1: ai quality =  " + fmt.Sprintf("%.2f", testUnit.aiQuality))
		genericTotalGoldMargin := genericTotalGoldDamageDealt - genericTotalGoldDamageTaken

		killMargin += genericTotalKillMargin * configInfo.genericWeights[testUnit.name]
		goldMargin += genericTotalGoldMargin * configInfo.genericWeights[testUnit.name]
		geoAvgGoldRatio = geoAvgGoldRatio * configInfo.genericWeights[testUnit.name] * (1.0 / float64(len(testUnits)) ) * genericTotalGoldRatio
		avgGoldRatio += genericTotalGoldRatio * configInfo.genericWeights[testUnit.name]

		_, _ = logFile.WriteString("#################################################")
		_, _ = logFile.WriteString("\n### Final Gold Ratio = " + fmt.Sprintf("%.2f", genericTotalGoldRatio) + " Kill Ratio = " + fmt.Sprintf("%.2f", genericTotalKillRatio))
		_, _ = logFile.WriteString("\n#################################################\n\n")
		_, _ = outFile.WriteString("#################################################")
		_, _ = outFile.WriteString("\n### Final Gold Ratio = " + fmt.Sprintf("%.2f", genericTotalGoldRatio) + " Kill Ratio = " + fmt.Sprintf("%.2f", genericTotalKillRatio))
		_, _ = outFile.WriteString("\n#################################################\n\n")
		_, _ = sumFile.WriteString(testUnit.name + ":\nGold Ratio = " + fmt.Sprintf("%.2f", genericTotalGoldRatio) + "\nKill Ratio = " + fmt.Sprintf("%.2f", genericTotalKillRatio) + "\n\n")

	}
	geoAvgGoldRatio = math.Pow(geoAvgGoldRatio, 1.0/float64(len(testUnits)))
	duration := time.Since(start)
	if verbose {
		fmt.Println()
		fmt.Println("All tests complete in " + strconv.Itoa(int(duration.Milliseconds())) + " ms.")
		time.Sleep(100)
		fmt.Println("Arithmetic Average Gold Ratio = " + fmt.Sprintf("%.2f", avgGoldRatio))
		//fmt.Println("Geometric Average Gold Ratio = " + fmt.Sprintf("%.2f", geoAvgGoldRatio))
		fmt.Println("Gold Margin = " + fmt.Sprintf("%.2f", goldMargin))
		fmt.Println("Kill Margin = " + fmt.Sprintf("%.2f", killMargin))

		fmt.Println("Summarized results can be found in: " + sumPath)
		fmt.Println("Abbreviated results can be found in: " + outPath)
		fmt.Println("Full results can be found in: " + logPath)

	}

	return testUnits
}






