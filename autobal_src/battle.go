package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)


func computeTerrainWeightedLosses(terrainBRs map[string]battleReport, terrains map[string]*terrain, configInfo configData) (float64, float64, float64, float64) {
	totalGoldDamageDealt := 0.0
	totalGoldDamageTaken := 0.0
	totalLossesDealt := 0.0
	totalLossesTaken := 0.0
	for terrain_winter, terrainBR := range terrainBRs {
		terrain := strings.Split(terrain_winter, "__")[0]
		winter := strings.Split(terrain_winter, "__")[1]
		totalGoldDamageDealt += terrainBR.weightedUnit2GoldLosses * terrains[terrain].freq * configInfo.winterWeights[winter]
		totalGoldDamageTaken += terrainBR.weightedUnit1GoldLosses * terrains[terrain].freq * configInfo.winterWeights[winter]
		totalLossesDealt += terrainBR.weightedUnit2ManLosses * terrains[terrain].freq * configInfo.winterWeights[winter]
		totalLossesTaken += terrainBR.weightedUnit1ManLosses * terrains[terrain].freq * configInfo.winterWeights[winter]
	}
	return totalGoldDamageDealt, totalGoldDamageTaken, totalLossesDealt, totalLossesTaken
}

func computeTerrainWeightedRatios(terrainBRs map[string]battleReport, terrains map[string]*terrain, configInfo configData) (float64, float64) {
	totalGoldDamageDealt, totalGoldDamageTaken, totalLossesDealt, totalLossesTaken := computeTerrainWeightedLosses(terrainBRs, terrains, configInfo)
	return totalGoldDamageDealt / totalGoldDamageTaken, totalLossesDealt / totalLossesTaken
}

func allTerrainBattles(unitA unit, unitB unit, terrains map[string]*terrain, configInfo configData, outFile *os.File, logFile *os.File) map[string]battleReport {
	terrainBRs := make(map[string]battleReport)
	for winter := range configInfo.winterWeights {
		for terrain, terrainObject := range terrains {
			terrainBRs[terrain + "__" + winter] = battle(unitA, unitB, terrainObject, winter)
			_, _ = logFile.WriteString("Terrain = " + terrain + "\n")
			_, _ = logFile.WriteString(getBRString(terrainBRs[terrain], false))
			_, _ = outFile.WriteString("Terrain = " + terrain + " Gold Ratio = " + fmt.Sprintf("%.2f", terrainBRs[terrain].goldRatio) +"\n")

		}
	}


 return terrainBRs
}

func battle(unitA unit, unitB unit, terrain *terrain, winter string) battleReport {

	unit1 := copyUnit(unitA)
	unit2 := copyUnit(unitB)
	postProcessUnit(&unit1, terrain.name, winter)
	postProcessUnit(&unit2, terrain.name, winter)

	width := calcWidth(&unit1, &unit2, terrain.width)
	unit2counteredMult := calcCounter(&unit1, &unit2)
	unit1counteredMult := calcCounter(&unit2, &unit1)
	battleOver := false
	tick := 0

	//printStatus(&unit1,&unit2,width, tick)
	for !battleOver {
		// Units attack each other
		attack(&unit1,&unit2, width, unit1counteredMult, unit2counteredMult)


		// Update width and counter power
		width = calcWidth(&unit1, &unit2, terrain.width)
		unit2counteredMult = calcCounter(&unit1, &unit2)
		unit1counteredMult = calcCounter(&unit2, &unit1)

		if unit1.currentSize <= marginOfError || unit2.currentSize <= marginOfError || tick > maxTicks {
			battleOver = true
		}

		tick++
	}

	battleAAR := createAfterAction(unit1, unit2, terrain.name)



	// conduct pursuit 1-2
	resetUnitsForPursuit(&unit1, &unit2)
	pursue(&unit1, &unit2, width, terrain.width)
	pursuit12AAR := createAfterAction(unit1, unit2, terrain.name)

	// conduct pursuit 2-1
	resetUnitsForPursuit(&unit1, &unit2)
	pursue(&unit2, &unit1, width, terrain.width)
	pursuit21AAR := createAfterAction(unit1, unit2, terrain.name)

	battleRep := createBattleReport(*battleAAR, *pursuit12AAR, *pursuit21AAR)

	return battleRep
}

func resetUnitsForPursuit(unit1 *unit, unit2 *unit) {

	unit1.currentSize = unit1.regimentSize * numberRegiments
	unit1.currentHP = unit1.currentSize * unit1.toughness

	unit2.currentSize = unit2.regimentSize * numberRegiments
	unit2.currentHP = unit2.currentSize * unit2.toughness
}

func pursue(unit1 *unit, unit2 *unit, width float64, widthMult float64) {
	// base daily pursuit damage
	dailyBasePursuitDamage := (basePursuitDamage * unit2.currentHP) / float64(pursuitDays)
	// minimum daily pursuit damage
	dailyMinPursuitDamage := (minPursuitDamage * unit2.currentHP) / float64(pursuitDays)

	for i :=  0; i < pursuitDays; i++ {
		// pursuit of pursuer counteracted by screen of retreater
		effectivePursuit := max(0.0, pursuitStatToDamageCoeff * unit1.pursuit * min(unit1.currentSize, width) - unit2.screen * min(unit2.currentSize, width))
		// add effective pursuit to daily base
		dailyPursuitDamage := dailyBasePursuitDamage + effectivePursuit
		// find how much HP of retreater is inside combat width
		retreaterAccessibleHP := unit2.toughness * min(unit2.currentSize, width)
		// apply as much damage as possible from the pursuer's attack to defender's HP
		retreaterProposedDmg := min(dailyPursuitDamage, retreaterAccessibleHP)
		// make sure pursuer's attack exceeds daily minimum
		retreaterActualDamage := max(retreaterProposedDmg, dailyMinPursuitDamage)

		// apply pursuer attack to retreater
		unit2.currentHP = max(0.0, unit2.currentHP - retreaterActualDamage)
		unit2.currentSize = max(0.0, unit2.maxSize * unit2.currentHP / unit2.maxHP)

		// recalc combat width
		width = calcWidth(unit1, unit2, widthMult)

		// stop pursuit if retreating unit is dead with reasonable margin
		if unit2.currentSize < marginOfError {
			break
		}
	}
}

func postProcessUnit(unit1 *unit, terrain string, winter string) {

	// add terrain bonuses
	unit1.damage += unit1.terrain[terrain][0]
	unit1.toughness += unit1.terrain[terrain][1]
	unit1.pursuit += unit1.terrain[terrain][2]
	unit1.screen += unit1.terrain[terrain][3]

	unit1.damage += unit1.winter[winter][0]
	unit1.toughness += unit1.winter[winter][1]
	unit1.pursuit += unit1.winter[winter][2]
	unit1.screen += unit1.winter[winter][3]

	if int(unit1.damage) < 0  {
		log.Fatal("negative or zero damage (" + strconv.Itoa(int(unit1.damage)) + ") for unit " + unit1.name + " in terrain " + terrain)
	} else if int(unit1.toughness) < 0 {
		fmt.Println("debug" + strconv.Itoa(int(unit1.damage)))
		log.Fatal("negative or zero toughness (" + strconv.Itoa(int(unit1.toughness)) + ") for unit " + unit1.name + " in terrain " + terrain)
	} else if int(unit1.pursuit) < 0 {
		log.Fatal("negative pursuit (" + strconv.Itoa(int(unit1.pursuit)) + ") for unit " + unit1.name + " in terrain " + terrain)
	} else if int(unit1.screen) < 0 {
		log.Fatal("negative screen (" + strconv.Itoa(int(unit1.screen)) + ") for unit " + unit1.name + " in terrain " + terrain)
	}

	// average damage and toughness values
	avgDT := (unit1.damage + unit1.toughness) / 2.0

	unit1.damage = avgDT
	unit1.toughness = avgDT

	// scale unit size to gold budget
	//fmt.Println("unit reg size = " + fmt.Sprintf("%.2f", unit1.regimentSize))
	unit1.maxSize = numberRegiments * unit1.regimentSize
	//fmt.Println("unit max size = " + fmt.Sprintf("%.2f", unit1.maxSize))
	unit1.currentSize = unit1.maxSize
	//fmt.Println("unit current size = " + fmt.Sprintf("%.2f", unit1.currentSize))
	unit1.maxHP = unit1.maxSize * unit1.toughness
	//fmt.Println("unit max HP = " + fmt.Sprintf("%.2f", unit1.maxHP))
	unit1.currentHP = unit1.maxHP
	//fmt.Println("unit current HP = " + fmt.Sprintf("%.2f", unit1.currentHP))
	//fmt.Println(unit1.regimentSize)
}

func calcCounter(unit1 *unit, unit2 *unit) float64 {
	if unit2.currentSize > 0 {
		unit1numReg := unit1.currentSize / unit1.regimentSize - math.Mod(unit1.currentSize, unit1.regimentSize) / unit1.regimentSize
		unit2numReg := unit2.currentSize / unit2.regimentSize - math.Mod(unit2.currentSize, unit2.regimentSize) / unit2.regimentSize
		counterPower1on2 := min(ratioForMaxCounter, unit1.counters[unit2.class] * unit1numReg / unit2numReg)
		counter1on2 := maxCounter * counterPower1on2 / ratioForMaxCounter
		return 1.0 - counter1on2
	} else {
		return 0.0
	}
}

func calcWidth(att *unit, def *unit, widthMult float64) float64 {
	return max(100.0,widthMult * (att.currentSize + def.currentSize) / 2.0)
}

func attack(att *unit, def *unit, width float64, attCounteredMult float64, defCounteredMult float64) {

	att_dmg := damageScalingFactor * min(att.currentSize, width) * att.damage * attCounteredMult
	defAccessibleHP := def.toughness * min(def.currentSize, width)
	defReceivedDmg := min(att_dmg, defAccessibleHP)

	def_dmg := damageScalingFactor * min(def.currentSize, width) * def.damage * defCounteredMult
	attAccessibleHP := att.toughness * min(att.currentSize, width)
	attReceivedDmg := min(def_dmg, attAccessibleHP)

	def.currentHP = max(0.0,def.currentHP - defReceivedDmg)
	def.currentSize = max(0.0,def.maxSize * def.currentHP / def.maxHP)

	att.currentHP = max(0.0,att.currentHP - attReceivedDmg)
	att.currentSize = max(0.0,att.maxSize * att.currentHP / att.maxHP)

}
