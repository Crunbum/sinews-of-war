package main

import (
	"fmt"
	"log"
	"os/user"
	"strings"
	"time"
)

func max(a float64, b float64) float64 {
	if a > b {
		return a
	} else {
		return b
	}

}

func min(a float64, b float64) float64 {
	if a < b {
		return a
	} else {
		return b
	}

}

func checkGenerics(genericUnits []unit, genericWeights map[string]float64) {

	genericsMap := make(map[string]int)
	for _, unit := range genericUnits {

		if _, ok := genericWeights[unit.name]; !ok {
			fmt.Println("Warning: generics file contains unrecognized generic unit: " + unit.name + ".\nThis unit will not be considered in the calculation of final gold ratio for test units")
		}
		if unit.regimentSize < 5.0 {
			log.Fatal("unit " + unit.name + " is of zero regiment size")
		}
		/*if abs(unit.maxSize - 100.0) > 0.1 {
			fmt.Println("Warning: unit " + unit.name + " is of nonstandard (stack != 100) size.")
		}*/
/*		if unit.gold <= 0.0 {
			log.Fatal("unit " + unit.name + " is missing a valid value for gold cost")
		}*/
		/*if abs(unit.gold - standardGold) > 0.01 {
			log.Fatal("Nonstandard gold value for unit " + unit.name + " ( " + fmt.Sprintf("%.2f", unit.gold) + " != " + fmt.Sprintf("%.2f", standardGold) + " ) ")
		}*/
		genericsMap[unit.name] = 1
	}

	sum := 0.0
	for generic, weight := range genericWeights {
		if _, ok := genericsMap[generic]; !ok {
			fmt.Println("warning: generic unit " + generic + " is missing in generics file")
		} else {
			sum += weight
		}

	}
	if abs(1.0 - sum) > 0.01 {
		fmt.Println("warning: generic unit weights don't sum to 1")
	}
	// added checks
	for _, unit := range genericUnits {
		if unit.name == "pikemen_unit" || unit.name == "heavy_infantry" {
			log.Fatal("invalid unit name " + unit.name)
		}
	}

}

func checkTests(testUnits []unit, recognizedTypes map[string]int) {

	for _, unit := range testUnits {
		//fmt.Println(unit.regimentSize)
		if recognizedTypes[unit.class] != 1 {
			log.Fatal("test file contains unrecognized generic unit type " + unit.class + " for unit " + unit.name)
		}
		/*if abs(unit.gold - standardGold) > 0.01 {
			log.Fatal("Nonstandard gold value for unit " + unit.name + " ( " + fmt.Sprintf("%.2f", unit.gold) + " != " + fmt.Sprintf("%.2f", standardGold) + " ) ")
		}*/
		if unit.regimentSize < 5.0 {
			log.Fatal("unit " + unit.name + " is of zero regiment size")
		}
	}
}

func abs(num float64) float64 {
	if num < 0 {
		return -num
	} else {
		return num
	}
}

func help() {
	thisUser, err := user.Current()
	if err != nil {
		panic(err)
	}
	time.Sleep(1000)
	fmt.Println("\nHello " + strings.Fields(thisUser.Name)[0] + "...")
	time.Sleep(1000)
	fmt.Println("\nThe below information should answer most common user questions.")
	fmt.Println()
	fmt.Println("DEMD Regiment Tools requires the following command line arguments:")
	fmt.Println("(1) Path to config file:")
	fmt.Println("The config file should contain the parameters \"province_terrain_folder\" and \"terrain_types_folder\"")
	fmt.Println("on separate lines, each followed by a semi colon and then the relevant path:")
	fmt.Println("e.g. \"province_terrain_folder;path\\to\\folder\"")

	fmt.Println("\nTerm Definitions:")
	fmt.Println("Gold Ratio: the gold value of the opposing soldiers killed by a unit during a battle divided by the gold" +
		" value of the soldiers in the unit that were killed.")
	fmt.Println("Kill Ratio: the number of opposing soldiers killed by a unit during a battle divided by the number" +
		" of soldiers in the unit that were killed.")
	fmt.Println("Terrain Weighted Gold Ratio: the gold ratios for the battles between two units weighted by the" +
		" relative frequency of each terrain type.")
	fmt.Println("Final Gold Ratio: the terrain weighted gold ratios for a unit against all generics weighted by the " +
		" relative importance of each generic type.")
	fmt.Println()
	log.Fatal("Insufficient arguments were provided")
}





func reverse2(numbers [][]float64)  {
	for i := 0; i < len(numbers)/2; i++ {
		j := len(numbers) - i - 1
		numbers[i], numbers[j] = numbers[j], numbers[i]
	}

}

func reverse3(numbers []string) {
	for i := 0; i < len(numbers)/2; i++ {
		j := len(numbers) - i - 1
		numbers[i], numbers[j] = numbers[j], numbers[i]
	}

}