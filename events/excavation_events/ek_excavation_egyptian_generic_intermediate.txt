﻿
namespace = ek_excavation_egyptian_generic_intermediate

ek_excavation_egyptian_generic_intermediate.0001 = {
	type = character_event
	title = ek_excavation_egyptian_generic_intermediate.0001.t
	desc = ek_excavation_egyptian_generic_intermediate.0001.desc
	theme = dungeon
	override_background = {
		event_background = excavation_interior_egyptian_01
	}
	
	left_portrait = {
		character = scope:story.story_owner
		animation = personality_bold
	}
	right_portrait = {
		character = scope:excavation_leader
		animation = martial
	}
	
	immediate = {
		excavation_story_event_standard_setup = yes
	}
	
	option = { 
		name = ek_excavation_egyptian_generic_intermediate.0001.a
		duel = {
			skill = diplomacy
			value = average_skill_rating
			# desc = demd_fire_relief_water
			20 = { # success	
				compare_modifier = {
					value = scope:duel_value
					multiplier = 0.5
				}
				alter_excavation_quality = { operation = add amount = small }
				
				desc = ek_excavation_egyptian_generic_intermediate.0001.success

			}
			20 = {
				alter_excavation_quality = { operation = subtract amount = small }
				desc = ek_excavation_egyptian_generic_intermediate.0001.failure
			}
		}		
	}
	option = { 
		name = ek_excavation_egyptian_generic_intermediate.0001.b
		remove_short_term_gold = activity_medium_gold_value
		alter_excavation_quality = { operation = add amount = small }
	}
	option = { 
		name = ek_excavation_egyptian_generic_intermediate.0001.c
		add_dread = minor_dread_gain
	}
}

ek_excavation_egyptian_generic_intermediate.0002 = {
	type = character_event
	title = ek_excavation_egyptian_generic_intermediate.0002.t
	desc = ek_excavation_egyptian_generic_intermediate.0002.desc
	theme = dungeon
	override_background = {
		event_background = excavation_interior_egyptian_02
	}
	
	left_portrait = {
		character = scope:story.story_owner
		animation = worry
	}
	
	immediate = {
		excavation_story_event_standard_setup = yes
	}
	
	option = { 
		name = ek_excavation_egyptian_generic_intermediate.0002.a
		
	}
	option = { 
		name = ek_excavation_egyptian_generic_intermediate.0002.b
		
	}
	option = { 
		name = ek_excavation_egyptian_generic_intermediate.0002.c
		# add_piety = medium_piety_gain
	}
}

		# sample effect
		#random_l
		#alter_excavation_quality = { operation = add amount = small }
		#alter_excavation_minor_artifacts = { operation = add amount = small }


