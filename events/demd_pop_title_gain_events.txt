﻿namespace = demd_title_gain

# Reset laws on county conquest
demd_title_gain.0001 = {
	hidden = yes
	type = empty
	immediate = {
		if = {
			limit = { is_ai = no }
			scope:title = {
				if = {
					limit = { tier = tier_county }
					
					
					# if player has fucked up edicts, unfuck them
					
					# reset to factory
					
					demd_set_manpower = { LEVEL = 2 }
					demd_set_tax_rate = { LEVEL = 2 }
					demd_set_religious_tolerance = { LEVEL = 1 }
					demd_set_cultural_tolerance = { LEVEL = 2 }
				
					# let AI make changes twice
					
					remove_variable = tax_rate_changed
					remove_variable = manpower_changed
					remove_variable = religious_tolerance_changed
					remove_variable = cultural_tolerance_changed
					
					setEdicts = yes
					
					remove_variable = tax_rate_changed
					remove_variable = manpower_changed
					remove_variable = religious_tolerance_changed
					remove_variable = cultural_tolerance_changed
					
					setEdicts = yes
						
						
					
				}					
			}
		}
	}
}

