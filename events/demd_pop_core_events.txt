﻿namespace = demd_population

# Run DEMD pop system game start pulse
demd_population.1000 = {
	hidden = yes
	type = empty
	immediate = {
	
		# Initialize provinces
		initialize_province_variables = yes
		faithEconomySubRoutine = yes # debug
		demd_preprocess_map = yes
		startupPulse = yes
		
		# run cycle
		annualPulse = { list = eligible_counties_all }
		tradeSubRoutine = yes
		faithEconomySubRoutine = yes
		cultureEconomySubRoutine = yes
		fertilitySubRoutine = yes
		setKnightCapacity = yes
		setRegimentCapacity = yes
		
		reset_migration_pulls
		top_up_urban_pop
		
		# set goods stockpile to equilibrium level
		top_up_goods_stockpiles = yes
		top_up_urban_pop = yes
		reset_migration_pulls = yes
				
		# run cycle
		annualPulse = { list = eligible_counties_all }
		tradeSubRoutine = yes
		faithEconomySubRoutine = yes
		cultureEconomySubRoutine = yes
		fertilitySubRoutine = yes
		setKnightCapacity = yes
		setRegimentCapacity = yes

		# set goods stockpile to equilibrium level
		top_up_goods_stockpiles = yes
		top_up_urban_pop = yes
		reset_migration_pulls = yes
		top_up_goods_districts = yes
		
		# add startup gold
		every_ruler = { 
			add_gold = startup_gold
		}
		
		# run cycle
		annualPulse = { list = eligible_counties_all }
		tradeSubRoutine = yes
		faithEconomySubRoutine = yes
		cultureEconomySubRoutine = yes
		fertilitySubRoutine = yes
		setKnightCapacity = yes
		setRegimentCapacity = yes
		
		# set goods stockpile to equilibrium level
		top_up_goods_stockpiles = yes
		top_up_urban_pop = yes
		reset_migration_pulls = yes
		
		# smooth over anything weird
		annualPulse = { list = eligible_counties_all }
		tradeSubRoutine = yes
		faithEconomySubRoutine = yes
		cultureEconomySubRoutine = yes
		fertilitySubRoutine = yes
		setKnightCapacity = yes
		setRegimentCapacity = yes
		
		# final adjustments
		# set goods stockpile to equilibrium level
		top_up_goods_stockpiles = yes
		top_up_urban_pop = yes
		reset_migration_pulls = yes		
		top_up_goods_districts = yes
		
		every_county = {
		
			set_variable = { name = faith_strength_attack value = faith_strength_attack_target }
			set_variable = { name = faith_strength_defense value = faith_strength_defense_target }
			
			set_variable = { name = culture_strength_attack value = culture_strength_attack_target }
			set_variable = { name = culture_strength_defense value = culture_strength_defense_target }
		}
		
		# smooth over anything weird
		annualPulse = { list = eligible_counties_all }
		tradeSubRoutine = yes
		faithEconomySubRoutine = yes
		cultureEconomySubRoutine = yes
		fertilitySubRoutine = yes
		setKnightCapacity = yes
		setRegimentCapacity = yes
		
		# final adjustments
		# set goods stockpile to equilibrium level
		top_up_goods_stockpiles = yes
		top_up_urban_pop = yes
		reset_migration_pulls = yes		
		top_up_goods_districts = yes
		
		every_county = {		
			remove_variable = tax_rate_changed
			remove_variable = manpower_changed
			remove_variable = religious_tolerance_changed
			remove_variable = cultural_tolerance_changed
		}
	}
}

# Run DEMD pop system annual things that cannot be easily amortized
demd_population.2000 = {
	hidden = yes
	type = empty
	immediate = {
		tradeSubRoutine = yes
	}
}

demd_population.2001 = {
	hidden = yes
	type = empty
	immediate = {
		faithEconomySubRoutine = yes
	}
}

demd_population.2002 = {
	hidden = yes
	type = empty
	immediate = {
		cultureEconomySubRoutine = yes
	}
}

demd_population.2003 = {
	hidden = yes
	type = empty
	immediate = {
		fertilitySubRoutine = yes
	}
}

demd_population.2004 = {
	hidden = yes
	type = empty
	immediate = {
		world_migration_end = yes
		world_migration_start = yes		
	}
}

demd_population.2005 = {
	hidden = yes
	type = empty
	immediate = {
		setKnightCapacity = yes
	}
}

demd_population.2006 = {
	hidden = yes
	type = empty
	immediate = {
		setRegimentCapacity = yes
	}
}





# Annual amortized things in separate file (written by machine)

