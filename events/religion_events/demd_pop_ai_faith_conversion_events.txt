﻿namespace = demd_pop_ai_faith

demd_pop_ai_faith.1001 = {
	type = character_event
	hidden = yes
	
	title = demd_pop_religious_conversion.1001.t
	desc = demd_pop_religious_conversion.1001.desc
	theme = faith
	
	immediate = {
		if = {
			limit = { 
				is_ai = yes 
				NOT = { faith = capital_province.faith }				
			}
			save_scope_as = convert
			
			set_variable = { name = best_realm_capital value = capital_province }
			every_neighboring_and_across_water_top_liege_realm_owner = {
				if = {
					limit = {
						capital_province = {
							faith = scope:convert.faith
							# var:faith_conversion_attack = { compare_value > scope:convert.capital_province.var:faith_conversion_defense }
							# var:faith_conversion_attack = { compare_value > scope:convert.var:best_realm_capital.var:faith_conversion_attack }
						}
					}
					set_variable = { name = best_realm_capital value = capital_province }
				}
			}
			if = {
				limit = { NOT = { var:best_realm_capital = capital_province } }
				
			}
		}
	}
}
